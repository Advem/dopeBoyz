<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('destination')->nullable();
            $table->string('title', 60)->nullable();
            $table->string('feature')->nullable();
            $table->string('production')->nullable();
            $table->string('album')->nullable();
            $table->string('description', 255)->nullable();
            $table->string('tag')->nullable();
            $table->string('image')->default('/images/defaultCover/defaultCover.png');
            $table->string('embed', 500)->nullable();
            $table->string('link_fb', 255)->nullable();
            $table->string('link_sc', 255)->nullable();
            $table->string('link_yt', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songs');
    }
}
