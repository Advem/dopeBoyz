<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('artist_name')->nullable();
            $table->string('image')->nullable();
            $table->string('role')->nullable();
            $table->string('description', 1000)->nullable();
            $table->string('link_fb', 500)->nullable();
            $table->string('link_sc', 500)->nullable();
            $table->string('link_yt', 500)->nullable();
            $table->string('link_tw', 500)->nullable();
            $table->string('link_ig', 500)->nullable();
            $table->integer('position')->default(99);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
