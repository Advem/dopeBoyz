[![N|Solid](https://dopeboyz.herokuapp.com/static/image.png)](https://dopeboyz.herokuapp.com)
# dopeBoyz
Music Platfrom created in Laravel

The purpose of creating the website was the need to unify the content of dopeBoyz artists who publish their work on many platforms. Thanks to the dopeBoyz platform, band members can add songs to the database and assign a specific song to many artists' subpages. When several artists worked on a given piece of music, the added song can be assigned to many artists, without the need to create a copy of the instance. Songs source files are fetched from SoundCloud service.

## Visit
https://dopeboyz.herokuapp.com

## Technologies
Laravel, PHP, MySQL, Sass, Bootstrap, jQuery 

## Documentation (polish)

Platforma internetowa zespołu muzycznego dopeBoyz, stworzona za pomocą framework'a języka PHP - **Laravel**, 
który łączy większość popularnych technologii webowych w jeden, złożony ekosystem.

Celem stworzenia wityrny była potrzeba ujednolicenia kontentu artystów wchodzących w skład zespołu dopeBoyz, którzy publikują swoją
twórczość na wielu platformach *(SoundCloud, YouTube, Facebook)* za pomocą wielu kont (konta prywatne, łączone, grupowe)
oraz stworzenie strony internetowej, która nie tylko umożliwi publikację utworów muzycznych, ale także zostanie pewną formą reklamy zespołu, kontaktu z 
audiencją oraz narzędziem pracy artystów.

Dzięki platformie **dopeBoyz**, członkowie zespołu (nazwani Moderatorami) mogą dodawać piosenki do bazy danych i przyporządkowywać konkretną piosenkę do wielu podstron artystów - podstrona, która wyświetla wszystkie piosenki przydzielone do danego artysty 
(w momencie, gdy nad danym utworem muzycznym pracowało kilku artystów, dodana piosenka może zostać przydzielona do wielu artystów, bez potrzeby tworzenia kopii instancji.

Piosenki na stronie wyświetlają się jako wylistowane elementy, a wciśnięcie konkretnej spowoduje uruchomienie odtwarzacza muzycznego, który jest klatką z serwisu *SoundCloud*. 
Zamiast stworzenia własnego odtwarzacza i pobieranie plików muzycznych z serwera *dopeBoyz*, postanowiono używać metody iframe z serwisu *SoundCloud*, dzięki któremu, każde odsłuchanie danej piosenki z platformy dopeBoyz powoduje zwiększenie pozycjonowania piosenki w serwisie *SoundCloud*, które popularyzuje zespół wśród użytkowników portalu *SoundCloud*.

Strona podzielona została na kilka części:
* */About* - podstrona wyświetlająca informacje o zespole oraz listę artystów, które są linkami do podstrony danego Artysty
* */Music* - podstrona wyświetlająca odtwarzacz najnowszego albumu zespołu oraz wylistowane piosenki wchodzące w jego skład, a także listę artystów
* */Music/{{Artysta}}* - podstrona wyświetlająca wszystkie piosenki przypisane do danego Artysty
* */Song/{{Tytuł Piosenki}}* - podstrona wyświetlająca daną Piosenkę
* */Album/{{Tytuł Albumu}}* - podstrona wyświetlająca piosenki wchodzące w skład danego Albumu
* */Graphics* - podstrona wyświetlająca grafiki, loga, okładki albumów zespołu
* */Shop* - podstrona aktualnie wyświetla licznik polubień strony zespołu na portalu Facebook
* */Contact* - podstrona wyświetla formularz kontaktowy, który wysyła wiadomość email na skrzynkę pocztową zespołu
* */Login* oraz */Register* - służą do logowania lub rejestracji konta użytkownika
* */Documentation* - podstrona wyświetlająca dokumentacje całego projektu oraz instrukcje dla moderatorów (aktualnie dostęp dla zalogowanych użytkowników)
* */Project* - podstrona wyświetlająca niepubliczne utwory, służąca jako miejsce pracy i testowanie utworów (dostęp tylko dla moderatorów)

* */Admin*:

(Administrator) podstrona panelu administracyjnego, która umożliwia administratorom dostęp do funkcjonalności tj.
1. dodawanie, usuwanie, edytowanie użytkowników
2. dodawanie, usuwanie, edytowanie artystów
3. dodawanie, usuwanie, edytowanie piosenek
4. zarządzanie powiązaniami między piosenkami a artystami
* (Moderator) podstrona panelu moderyzacji, która umożliwia moderatorom dostęp do funkcjonalności tj.
1. edytowanie artysty, którego moderator jest właścicielem
2. dodawanie nowych piosenek
3. edytowanie piosenek dodanych przez moderatora
4. tworzenie powiązań piosenek dodanych przez moderatora z innymi artystami
5. usuwanie powiązań piosenek artysty, którego moderator jest właścicielem
* */Backup* - podstrona wyświetlająca listę zapytań do bazy danych, tworzonych w momencie dodawania i edytowania piosenek (dostęp tylko dla administracji)

# Podstrony

### Home Page
Strona startowa **Home Page**, to prosta w grafice strona, która ma na celu wyświetlenie użytkownikowi menu **[Home Menu]** dostępu do innych podstron **[Sub Page]** takich jak: *(About, Music, Graphics, Shop[zostanie ukryte], Contact)*, a także po rozwinięciu dalszej cześci menu, za pomocą znaku białego trójkąta, do podstron *(Log in, Registry, Support)*. Pasek menu jest responsywny, tj. dostosowuje się do rozmiaru ekranu użytkownika i przy szerokości ekranu *576px*, pasek menu przybiera drugą formę, gdzie zamiast wylistowanych przycisków podstron w jednym wierszu, wyświetla się ikona białej strzałki, która po wciśnięciu rozwija pasek menu, którego przyciski ustawione są pionowo. Po wciśnięciu przycisku, użytkownik zostaje przekierowany do danej podstrony i wyświetlony zostanie, na samej górze strony, czarny pasek menu **[Main Menu]** zawierający logo (którego wciśnięcie spowoduje przekierowanie do **Home Page**) oraz identyczne przyciski podstron jak na stronie **Home Page**, który również jest responsywny zależnie od szerokości ekranu użytkownika i dodatkowo jest zamieszczony na stałe w górnej części ekranu (scrollowanie strony w dół nie spowoduje ukrycia paska **Main Menu**). Pasek **Main Menu** wyświetla dodatkową grafikę (biały pasek) pod przyciskiem nazwy aktualnie odwiedzanej podstrony. Na dole strony znajdują się linki do *SocialMedia*, których adres URL jest przechowywany w bazie danych *(tablica: Members)* pod nazwą **[Member Name]** *"dopeBoyz"*. Linki te są skonstruowane w taki sposób, że po wciśnięciu odpowiedniego przycisku oznaczonego symbolem portalu, przeglądarka użytkownika otworzy nową kartę, nie powodując zamknięcia karty strony **dopeBoyz**. W tle strony startowej widnieje obraz loga, którego zarówno szerokość jak i pozycja również skaluje się wraz ze zmianą wielkości ekranu użytkownika i przyjmuje tendencje do pozycjonowania na środku ekranu dla ekranów tabletów, komputerów, telewizorów lub pozycjonowania w górnej części dla smartphonów.

### About
Podstrona About wyświetla statyczny tekst opisu zespołu dopeBoyz oraz nazwy artystów *(tablica: Artists)* - dynamiczne linki do podstrony artysty, które po wciśnięciu przekierowują użytkownika do strony: */music/ArtistName*.

### Music
Podstrona **Music** wyświetla odtwarzacz muzyki najnowszego albumu zespołu, wylistowane piosenki **[Songs]** wchodzące w skład najnowszego albumu oraz dodatkowy, responsywny pasek menu **[Artist Menu]**, którego przyciski to nazwy artystów z bazy danych *(tablica: Artists)*, którego wciśnięcie spowoduje przekierowanie użytkownika do podstrony danego artysty oraz nałożenie dodatkowej grafiki pod jego nazwą. Treścią podstrony **ArtistName** są wylistowne utwory muzyczne **[Songs]**, przypisane do konkretnego artysty.

### Graphics
Podstrona **Graphics** wyświetla wylistowane obrazki w formie kafelków z bazy danych *(tablica: Graphics)*. Wciśnięcie kafelka obrazu powoduje powiększenie obraz na ekranie oraz wyświetlenie tytułu ilustracji.

### Shop
Podstrona **Shop** wyświetla tekst oraz dynamiczną grafikę strony *Facebook* zawierającą przycisk *Like* oraz *Share* strony *DOPEBOYZFAMILIA* na portalu *Facebook* oraz linki do *Social Media* w dolnej części storny.

### Contact
Podstrona **Contact** wyświetla formularz kontaktowy, który wysyła wiadomość na skrzynkę pocztową. Aby wiadomość została wysłana, użytkownik musi wypełnić poprawnie 3 pola (imię użytkownika, adres email użytkownika, tekst wiadomości). Jeżeli wiadomość została wysłana poprawnie, strona przeładuję się i wyświetli komunikat o powodzeniu. W przeciwnym razie zostanie wyświetlony komunikat o nieprawidłowym wypełnieniu formularza.

### Project
Podstrona **Project**, do której mają dostęp tylko użytkownicy o typie **Moderator** oraz **Admin**, wyświetla elemnty **Song**, których wartość parametry **[Destination]** jest równa *"Project"*. Służy ona do publikacji nowych, niedokończonych utworów muzycznych między członkami zespołu, których pliki źródłowe instenija na serwerze portalu *SoundCloud* jako niepubliczne, a ich dodanie na podstroną **Project** umożliwi zespołowi *dopeBoyz* przesłuchanie ich.

### Documentation
Podstrona **Documentation** zawiera tekstowy opis całego serwisu. Aktualnie dostęp do tej podstrony ma każdy zalogowany użytkownik, nie mniej jedak dostęp do tejże podstrony, będzie ograniczny do użytkowników o typie **Moderator** oraz **Admin** w późniejszym czasie.

# Elementy

## Artist
Element **Artist** to zapis w bazie danych *(tablica: Artists)*, którego parametry to:
nazwa **[Artist Name]**, powiązanie z użytkownikiem **[User ID]** oraz powiązanie z piosenką **[Song ID]**. Do każdego elementu **Artist** można przyporządkowany wiele elementów **Song**. Każdy nowo utworzony **Artist** zostaje wyświetlany na pasku **Artist Menu**, który widnieje na podstronie **Music** i nie jest przypisany do żadnego użytkownika **User**.
Zarówno dodanie nowego elementu **Artist** jak i przypisanie go do konkretnego użytkownika dokonywane jest przez administracje **(Admin User)**. Element ten może być powiązany tylko z jednym użytkownikiem. Do każdego elementu **Artist** może być przyporządkowanych wiele elementów **Songs**. Wszystkie przypisane utwory muzyczne **Songs** danego **Artist** są wyświetlane na podstronie **Music** wielkimi literami. Link do podstrony artysty wygląda następująco: */music/**ArtistName***, który może zostać wyświetlony przez dowolnego internaute. Dopuszczalne jest skrócenie linku do postaci: */music/**ArtistName*** w celach estetycznych, które może prowadzić do spowolnieniem połączenia z serwerem. Wielkość liter nazwy **ArtistName** jest nieistotna, nie wpływa na połączenie do podstrony przez link i może być stosowana dowolnie w celach estetycznych.

## Song
Element **Song** to zapis w bazie danych *(tablica: Songs)*, którego parametry to: tytuł utworu **[Title]**, lista wokalistów **[Feature]**, lista producentów **[Production]**, tytuł albumu **[Album]**, opis utworu **[Description]**, gatunek utworu **[Tag]**, link obrazka utworu lub albumu **[Image]**, link do odtwarzacza **[Embed]**, linki do utworu na portalach społecznościowych: **[LinkFB]**, **[LinkSC]**, **[LinkYT]**, powiązanie z elementem **[Artist ID]**, powiązanie z użytkownikiem **[User ID]**, data utworzenia utworu **[Created At]**, data aktualizacji parametrów elementu **[Updated At]** oraz miejsce docelowe **[Destination]**. Zarówno dodanie nowego elementu **Song** jak i przypisanie go do konkretnego elementu **Artist** dokonywane jest przez użytkowników z poziomem autoryzacji **Moderator User**.

### Song Destination
Parametr **Destination** określa miejsce docelowe elementu **Song** i przyjmuje 3 wartości *("Music"*, *"Project"*, *"Album")*.
- Wartość *"Music"* jest domyślną wartością elementu **Song** podczas dodawania nowego elementu. Tylko te elementy **Song**, które mają wartość parametru **[Destination]** równą *"Music"*, mogą być przypisane do danego artysty i mogą zostać wyświetlane na podstronach **Music** oraz **Artist**.
- Wartość *"Project"* określa przynależność elementu **Song** do podstrony **Project** znajdującej się pod adresem *"/project"*, która może zostać wyświetlona tylko użytkownikom o typie **Moderator** oraz **Admin**. Elementy o wartości *"Project"* nie mogą zostać przypisane do żadnego artysty. 
- Wartość *"Album"* określa element **Song** jako element do wyświetlenia na podstronie **Music** jako rozbudowany player całego albumu za pomocą linku parametru **[Embed]**. W bazie powinien znajdować się tylko jeden taki element, który będzie wyświetlany jako "Najnowszy Album". Wszystkie elementu **Song**, których parametr **[Album]** jest taki sam jak parametr **[Title]** elementu o wartości parametrzu **[Destination]** równą *"Album"* zostaną wyświetlone na podstronie **Music**. W celu aktualizacji najnowszego albumu należy edytować istniejący element **Album** lub stworzyć nowy, usuwając poprzedni. W przypadku, gdy w bazie danych nie istnieje żaden element **Album**, podstrona **Music** nie zostanie załadowana.

### Song Title
Parametr **Title** to tytuł utworu, który powinien się składać z maksymalnie *30* znaków oddzielanych spacjami, gdyż w przeciwnym wypadku pozostałe parametry zaburzą estetyczny układ elementu. Parametr ten również powinien się składać z maksymalnie jednego wyrazu posiadającego *12* symboli, gdyż w przeciwnym wypadku tytuł nie zostanie wyświetlony w pełnej postaci na urządzeniach o szerokości ekranu *360px*. Tytuł, ze względów estetycznych, powinien składać się z wyrazów zapoczątkowanych wielką literą (*"Song Title Looks Nice Now"* ). Polskie znaki na ten moment nie są akceptowalne z powodu błędnego odczytu adresów URL.

### Song Feature
Parametr **Feature** zawiera informację o wokalistach. Ze względów estetycznych treść powinna zawierać nazwy wokalistów zapoczątkowanych wielką literą, oddzielonych małą literą *"**x**"* (*"Artist1 **x** Artist2 **x** Artist3"* ).

### Song Production
Parametr **Production** zawiera informację o twórcach podkładu muzycznego. Ze względów estetycznych treść powinna zawierać nazwy producentów zapoczątkowanych wielką literą, oddzielonych małą literą *"**x**"* (*"Producent1 **x** Producent2 **x** Producent3"* ).
Obydwa powyższe parametry wyświetlane są w układzie elementu **Song** w jednej linii i oddzielone są od siebie wyrazem *"prod."* w celu rozróżnienia twórców. (*"Artist1 **x** Artist2 **prod.** Producent1 **x** Producent2"* ).

### Song Album
Parametr **Album** służy jedynie do grupowania piosenek i nie jest wyświetlany w układzie elementu **Song**. Jeżeli element **Song** posiada wartość parametru **Album**, która jest taka sama jak tytuł **[Title]** elementu, którego parametr **[Destination]** ma wartość *"Album"* zostanie on wyświetlony na podstronie **Music** jako składowa "najnowszego albumu". Polskie znaki na ten moment nie są akceptowalne z powodu błędnego odczytu adresów URL.

### Song Tag
Parametr **Tag** określa gatunek muzyczny utworu. Treść parametru powinna składać się z maksymalnie *11* symboli zapoczątokowana wielką literą. W przeciwnym wypadku treść parametru zostanie wysunięta po za obramowanie, powodując nieład układu elementu. Pusta wartość parametru **[Tag]** spowduje wyświetlenie pustej grafiki w układzie elementu **Song**.

### Song Description
Parametr **Description** zawiera infromację opisującą utwór muzyczny. Treść opisu nie powinna przekroczyć *255* znaków. Opis wyświetlany na ekranach o szerokości *360px* zostanie skrócony do *80* znaków w celu zachowania układu elementu. Wraz ze zwiększaniem szerokości ekranu limit znaków będzie zachowywał tendencję rosnącą aż do momentu, w którym zostanie wyświetlona cała treść opisu.

### Song Image
Parametr **Image** to adres linku obrazu, który zostanie wyświetlony w układzie elementu jako kwadrat o wymiarach *150x150 pikseli*. Kliknięcie w obraz elementu **Song** spowoduje uruchomienie odtwarzacza **Player**, który jest bezpośrednio połączony z parametrem **[Embed]**. Adres obrazu może być dowolny, pod warunkiem, że plik obrazu jest dostępny globalnie. Własne grafiki należy umieścić na portalach społecznościowych lub na dysku wirtualnym w trybie publicznym. Uzyskanie adresów grafik z serwisów internetowych można wykonać poprzez odnalezienie ukrytego adresu za pomocą silnika przeglądarki:
(używając *Google Chrome*) należy uruchomić tryb programisty skrótem klawiszowym *Ctrl+Shift+I* lub kliknąć prawym przyciskiem myszy na obraz i wybierając opcje *Zbadaj*, następnie przejść do zakładki Elementy/Elements i uruchomić, narzędzie podglądania za pomocą skrótu klawiszowego *Ctrl+Shift+C* lub pierwszego przycisku na pasku zakładek i wciśnięcie lewym przyciskiem myszy porządany obraz. Zostanie wówczas podświetlona linia kodu z adresem linku.
Należy skopiować zawartość cudzysłowa funkcji *url("**SongImage**");* i przekopiować ją do formularza. Przykładowo podświetlony zostanie kod:
*span style="background-image: url("**https://i1.sndcdn.com/artworks-000452494287-txctvz-t500x500.jpg**"); width: 100%; height: 100%; opacity: 1; ..."*
Adresem grafiki jest link wyróżniony pogrubionym tekstem.

### Song Embed
Parametr **Embed** służy do łączenia elementu **Song** z plikiem utworu muzycznego znajdującego się na serwerze *SoundCloud*. Aby użytkownik mógł uruchomić **Player** w celu przesłuchania pliku muzycznego danego elementu należy do wartości Parametru **[Embed]** przypisać wartość jaką jest adres linku. Uzyskanie adresu z serwisu *SoundCloud* wygląda następująco:
Należy odnaleźć utwór muzyczny na serwisie i otworzyć jego pełną stronę, której adres przykładowo wygląda w ten sposób:
*https://soundcloud.com/xannytrejoofroswell/memento-mori*
Następnie należy przycisnąć przycisk (*Udostępnij *), wybrać opcję (*Umieść na stronie *), zaznaczyć środkowy styl odtwarzacza (*Classic embed *), wybrać kolor odtwarzacza, który pasuje do grafiki elementu, odznaczyć opcję (*Włącz automatyczne odtwarzanie *) i przekopiować adres z pola (*Kod *). Następnie należy skopiować tylko zawartość funkcji *src="**SongEmbed**"* (zaznaczone wygrubionym tekstem; spacje zostały umieszczone celowo) przykładowo:
*<\iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="**https:// w.soundcloud.com/ player/ ?url=https%3A// api.sound cloud.com/ tracks/539263578& color=%232c2c14& auto_play=false& hide_related=false& show_comments=true& show_user=true& show_reposts=false& show_teaser=true**"><\/iframe>*
a następnie wkleić adres do pola formularza parametru **[Embed]**. **Player** aktywowany jest poprzez wciśnięcie obrazka elementu **[Song Image]**.

### Song Links
Paramtery **Links** *SocialMedia* służą do łączenia elementu **Song** z portalami społecznościowymi za pomocą hiperłączy. Treścią parametru jest adres URL pozyskiwany z adresu platformy. Links są wyświetlane w układzie elementu **Song** za pomocą ikon - konkretny parametry jest oznakowany logiem portalu społecznościowego, a wciśnięcie go przez użytkownika przekieruje go na stronę docelową, otwierając nową kartę w przeglądarce, nie powodując zamknięcia karty strony. W przypadku, gdy Paramtery **[Links]** jest pusty tj. jego wartość nie posiada treści, nie zostanie on wyświetlony w układzie **Song**.

### Song Buttons
W skład elementu **Song** wchodzą również dodatkowe przyciski, które znajdują się tuż obok **Song Links**:

- Ikona Nuty - przycisk przekierowujący użytkownika do podstrony, na której znajduje się tylko dany element Song pod adresem: */song/SongTitle*
- Ikona Płyty - przycisk przekierowujący użytkownika do podstrony, na której znajdują się wszystkie elementy Song, które posiadają tą samą wartość parametru **[Album]** pod adresem: */album/SongAlbum*
- Ikona Edycji - przycisk przekierowujący do panelu edycji elementu Song, wyświetlana tylko, gdy użytkownik jest właścicielem tego elementu.
- Ikona Kosza - przycisk usuwający elementu Song, wyświetlana tylko, gdy użytkownik jest właścicielem tego elementu.

### Song Created At
Parametr **Created At** służy do przechowywania dokładnego czasu powstania elementu **Song**, dzięki czemu, w układzie **Song** generowany jest wpis wyświetlający ilość czasu, jaka upłyneła od daty powstania elementu. Wyświetlanie czasu wygląda następująco:
jeżeli od powstania elementu upłynął czas mniejszy niż jedna minuta - wyświetlona zostanie ilość sekund oraz napis "*sec ago*";
jeżeli od powstania elementu upłynął czas mniejszy niż jedna godzina - wyświetlona zostanie ilość minut oraz napis "*mins ago*";
jeżeli od powstania elementu upłynął czas mniejszy niż jeden dzień - wyświetlona zostanie ilość godzin oraz napis "*hours ago*";
jeżeli od powstania elementu upłynął czas mniejszy niż jeden miesiąc - wyświetlona zostanie ilość dni oraz napis "*days ago*";
jeżeli od powstania elementu upłynął czas mniejszy niż jeden rok - wyświetlona zostanie ilość miesięcy oraz napis "*months ago*";
jeżeli od powstania elementu upłynął czas większy lub równy jeden rok - wyświetlona zostanie ilość lat oraz napis "*years ago*";
W przypadku, gdy ilość czas jak upłynęła od czasu stworzenia elementu jest równa *1* tzn. "*jeden miesiąc*",
wyświetlony zostanie napis "*month ago*" zamiast napisu "*months ago*" oraz analogicznie dla pozostałych przypadków.
Parametr **[Created At]** jest generowany automatycznie w momencie dodawnia nowego elementu **Song** do bazy danych *(tablica: Songs)*, nie mniej jednak, w celu usprawnienia bazy danych, w których utwory muzyczne artystów zostały stworzone o wiele wcześniej, dodano możliwość zmiany wartości paramteru **[Created At]** na dowolną w formacie RRRR-MM-DD hh:mm:ss "*1997-09-13 03:14:27 *", którą można ustawić w panelu aktualizacji elementu **Song**.

### Song Fresh
W przypadku, gdy wartość parametru **[Created_At]** wynosi *mniej niż 7 dni* w układzie elementu **Song** zostaniej wyświetlona dodatkowa grafika z napisem *Fresh*.

### Song Updated At
Parametr **Updated At** służy do przechowywania dokładnego czasu jaki upłynął od ostatniej edycji elementu **Song**, dzięki czemu, administracja seriwus otrzymuje informację o zmianie parametrów elementu. Parametr **[Updated At]** jest generowany automatycznie w momencie aktualizacji istniejącego elementu **Song** w bazie danych *(tablica: Songs)*.

### Song Artist ID
Parametr **Artist ID** służy do przechowywania informacji, który element **Artist** jest powiązany z elementem **Song**, dzięki czemu dany element **Song** zostanie wyświetlony w liście elementów **Songs** danego elementu **Artist** na podstronie **Music** konkretnego artysty ("*/music/**ArtistName*** "). Treścią tego parametru jest dowolny, istniejący w bazie danych *(tablica: Artist)* element **Artist**. Każdy element **Song** może być przyporządkowany wielu elementom **Artist**. Przyporządkowanie konkretnemu elementowi **Song** treści parametru **[Artist ID]** jest dokonywane poprzez panel **Artist Songs** konkretnego elementu **Artist**.

### Song User ID
Parametr **User ID** służy do przechowywania informacji, który użytkownik **User** jest powiązany z elementem **Song**, dzięki czemu dany element **Song** może zostać edytowany, 
aktualizowany tylko przez konkretnego użytkownika **User**. Treścią tego parametru jest tylko konkretny, istniejący w bazie danych *(tablica: Users)* użytkownik **User**.
Każdy element **Song** może być przyporządkowany tylko jednemu użytkownikowi **User**. Przyporządkowanie konkretnemu elementowi **Song** treści parametru **[User ID]** 
jest dokonywane automatycznie podczas dodawania nowego elementu **Song** do bazy danych *(tablica: Songs)* przez aktualnie zalogowanego użytkownika **User**, 
którego typ autoryzacji ma okreslony priorytet jako **Moderator User**.

# Użytkownicy

### Register
Każdy użytkownik może utworzyć konto za pomocą przycisku **Register**, które zostanie zapisane w bazie danych *(tablica: Users)*. Do utworzenia konta użytkownik musi podać nazwe **[User Name]**, która, w przypadku moderatorów, będzie widoczna pod piosenką stworzoną przez użytkownika, adres email, który posłuży do logowania się do serwisu, oraz hasła dostępu. Po stworzeniu nowego konta, użytkownik zostaje przekierowany do formularza profilowego **[Profile]**. Każdy użytkownik, który utworzył konto, otrzymuje przypisany typ **[Default User]**, którego dostęp do zaawansowanych funkcjonalności jest ukryty.

### Log In
Użytkownik, który posiada konto zapisane w bazie danych, może zalogować się do swojego konta za pomocą wcześniej podanego loginu i hasła poprzes przycisk **Log In**. W przypadku nieprawdiłowo wpisanych danych, wyświetlany jest komunikat, a proces logowania kończy się niepowodzeniem. Użytkownik może zresetować swoje hasło wciskając link w formularzu logowania, który spowoduje przesłanie, na podany adres email wiadomości z instrukcjami do stworzenia nowego hasła. Zarówno pasek **Home Menu** jak i **Main Menu**, nie wyświetlają przycisków *"Registry"* oraz *"Log in"* w momencie, gdy użytkownik jest zalogowany, natomiast, gdy użytkownik zalogował się do serwisu, na paskach menu pojawiają się przyciski *"Profile"* oraz *"Log Out"*.

### Log Out
Użytkownik posiadający konto, może wylogować się z sesji serwisu za pomocą przycisku **Log Out**, a jego dane pozostaną ukryte do momentu ponownego zalogowania.

### Profile
Podstrona **Profile** wyświetla użytkownikowi formularz, dzięki którem może on zapisać swoje dane osobiste do bazy danych. Funkcjonalność ta jest przygotowana na wypadek, gdy serwis zaoferuje użytkownikom możliwość zakupu produktów.

### Type
Parametr **Type** użytkownika określa jego poziom autoryzacji. Dostępne są 3 warianty parametru **Type**: *(Default User, Moderator User, Admin User)*.
- Typ **Default** jest przypisywany każdemu nowemu użytkownikowi, który daje użytkownikowi dostępn do podstrony Profile, gdzie może zaktualizować swoje dane osobowe. 
- Typ **Moderator** jest przypisany do użytkownika tylko przez użytkownika o typie **Admin** i umożliwia użytkownikowi ograniczony dostęp do zaawansowanych funkcjonalności i edycji skonkretyzowanych elementów bazy danych. 
- Typ **Admin** należy do administracji serwisu i umożliwia użytkownikowi dostęp do wszystkich funkcjonalności.

# Moderyzacja

### Panel Moderatora
W Panelu Moderatora znajdują się:

1. lista elementów **Artist**, w której skład wchodzi:
- Nazwa Artist
- Nazwa User, który jest właścicielem elementu Artist
- Liczba posiadanych elementów Song
- Przycisk edycji elementu Artist (gdy User jest właśicielem tego elementu)
- Przycisk przekierowujący do podstorny artysty */music/ArtistName*
- Przycisk przekierowujący do Panelu Artysty

2. Formularz dodawania nowych elementów **Song**

3. Lista elementów **Song**, których tło zależne jest od właściciela elementu, określone kolorem:
- Niebieski - User jest właścicielem elementu
- Czerwony - User nie jest właścicielem elementu
- Biały - Element jest traktowanych jako **Album**, wyświetlany na podstronie **Music** *jako "Najnowszy Album"*
- Artist

Administrator może dodać nowego artystę do bazy danych, który później połączony zostanie z jednym użytkownikiem o typie **Moderator**, który staję się właścielem danego elementu **Artist**. Może on wówczas edytować nazwę oraz odłączać elementy **Song** od swojego elementu **Artist**.

### Panel Artist
Panel Artist danego elementu **Artist** składa się z dwóch części: listy aktualnie przypisanych elementów **Song** oraz listy elementów **Song**, których **User** jest właścicielem. Użytkownik, który jest właścicielem elementu **Artist** może odłączyć wszystkie elementy Song ze swojego Artist. Użytkownik, który nie jest właścicielem elementu **Artist** może odłączyć tylko te elementy Song, których jest właścicielem. Użytkownik może dołączać do każdego elementu Artist elementy Song, których jest właścicielem, ale tylko te, które posiadają parametr **[Destination]** równy *"Music"*. Dołączanie i odłączanie elementów Song to elementów Artist wykonuje się za pomocą przycisków po prawej stonie elementów Song. W momencie, gdy użytkownik nie ma uprawnień do wykonania danej operacji, przyciski te nie zostaną wyświetlone.

### Panel Song
Użytkownik, może edytować i usuwać tylko te elementy **Song**, których jest właścicielem.

Copyright (C) 2019 Adam 'Advem' Drabik
