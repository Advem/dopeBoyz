<?php

// Home routes
Route::get('/', 'SubpageController@index');
Route::get('/#', 'SubpageController@index');
Route::get('/home', 'SubpageController@index');

//Contact routes
Route::get('/contact', 'SubpageController@contact');
Route::put('/contact/send', 'MailController@sendContactMail');

//User routes
Route::get('/user', 'UserController@index')->name('user'); //User panel
Route::patch('/user/{user}/update',  ['as' => 'users.update', 'uses' => 'UserController@update'])->middleware('user_group:Admin'); //User update route
Route::get('/user/{userFind}', 'UserController@find')->middleware('user_group:Admin');
Route::get('/user/{user}/delete', 'UserController@destroy')->middleware('user_group:Admin');
Route::get('/user/{user}/edit', 'UserController@edit')->middleware('user_group:Admin');
Route::get('/admin', 'UserController@admin')->middleware('user_group:Admin,Moderator')->name('admin')->name('panel');

//Music routes
Route::get('/music', 'ArtistController@index')->name('music');
Route::get('/music/{artist}', 'SongController@index')->name('artist');
Route::get('/song/{song}', 'SongController@song')->name('song');
Route::get('/album/{album}', 'SongController@album')->name('album');

//Artist routes
Route::get('/artist/{artist}/delete', 'ArtistController@destroy')->middleware('user_group:Admin');
Route::put('/artist/add', 'ArtistController@store')->middleware('user_group:Admin');
Route::get('/artist/{artist}/edit', 'ArtistController@edit')->middleware('user_group:Admin,Moderator');
Route::patch('/artist/{artist}/update', ['as' => 'artist.update', 'uses' => 'ArtistController@update'])->middleware('user_group:Admin,Moderator');

//Artist edit songs
Route::get('/artist/{artist}/songs', 'ArtistController@songs')->middleware('user_group:Admin,Moderator');
Route::get('/artist/{artist}/disassoc/{song}', 'ArtistController@disassoc')->middleware('user_group:Admin,Moderator');
Route::get('/artist/{artist}/assoc/{song}', 'ArtistController@assoc')->middleware('user_group:Admin,Moderator');

//Song routes
Route::get('/song/{song}/delete', 'SongController@destroy')->middleware('user_group:Admin,Moderator');
Route::put('/song/add', 'SongController@store')->middleware('user_group:Admin,Moderator');
Route::get('/song/{song}/edit', 'SongController@edit')->middleware('user_group:Admin,Moderator');
Route::patch('/song/{song}/update', ['as' => 'song.update', 'uses' => 'SongController@update'])->middleware('user_group:Admin,Moderator');

//Members routes
Route::get('/about', 'SubpageController@about')->name('about');
Route::get('/shop', 'SubpageController@shop')->name('shop');
Route::get('/graphics', 'SubpageController@graphics');
Route::get('/documentation', 'SubpageController@documentation');
Route::get('/project', 'SubpageController@project')->middleware('user_group:Admin,Moderator')->name('project');
Route::get('/backup', 'SubpageController@backup')->middleware('user_group:Admin,Moderator');
Route::get('/support', 'SubpageController@support');

Auth::routes();