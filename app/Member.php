<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'name', 'artist_name', 'image', 'role', 'description', 'link_fb', 'link_sc', 'link_yt', 'link_tw', 'link_ig', 'position'
    ];
}
