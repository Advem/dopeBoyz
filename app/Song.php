<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    protected $fillable = [
        'user_id', 'title', 'destination', 'feature', 'production', 'album', 'description', 'tag', 'embed', 'link_fb', 'link_sc', 'link_yt', 'image'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function artist(){
        return $this->belongsToMany(Artist::class);
    }
}
