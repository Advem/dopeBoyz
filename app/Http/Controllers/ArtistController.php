<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\User;
use App\Artist;
use App\Song;


class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['count'] = Song::count();
    
        if($data['count'] > 0) {
            $data['newestAlbum'] = Song::where('songs.destination', 'Album')->orderBy('songs.created_at', 'DESC')->first();
            $data['albums'] = Song::where('songs.destination', 'Album')->get();
            $data['songs'] = Song::where('songs.album', $data['newestAlbum']->title)->where('songs.destination', 'Music')
                ->orderBy('songs.created_at', 'DESC')->get();
        }
        $data['artists'] = Artist::orderBy('artists.position', 'ASC')->get();
        return view('music.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->isAdmin()){
            Artist::create($request->all() + ['user_id' => 0]);
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function show(Artist $artist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function edit(Artist $artist)
    {
        if(Auth::user()->isAdmin()){
            return view('artist.edit', compact('artist'));
        }

        else if(Auth::user()->isModerator() && Auth::user()->id === $artist->user_id){
            return view('artist.edit', compact('artist'));
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function update(Artist $artist)
    {
        if(Auth::user()->isAdmin()){
            $artist->artist_name = request('artist_name');
            $artist->position = request('position');
            $artist->user_id = request('user_id');
            $artist->save();
        }

        else if(Auth::user()->isModerator() && Auth::user()->id === $artist->user_id){
            $artist->artist_name = request('artist_name');
            $artist->save();
        }

        return redirect('/admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artist $artist)
    {
        if(Auth::user()->isAdmin()){
            $artist->delete();
        }
        return back();
    }

    public function songs(Artist $artist){
        if(Auth::user()->isAdmin()){
            $data['artist'] = $artist;
            $data['songs'] = Song::orderBy('songs.created_at', 'DESC')->get();
            return view('artist.songs', compact('data'));
        }

        else if(Auth::user()->isModerator() /* && Auth::user()->id === $artist->user_id */){
                $data['artist'] = $artist;
                $data['songs'] = Song::where('user_id',Auth::user()->id)->orderBy('songs.created_at', 'DESC')->get();
                return view('artist.songs', compact('data'));
        }

        return back();
    }

    public function disassoc(Artist $artist, Song $song){

        if($song->destination == 'Music') {

        if(Auth::user()->isAdmin()){
            $artist->songs()->detach($song);
        }

        else if(Auth::user()->isModerator())
        if($song->user->id === Auth::user()->id || $artist->user->id === Auth::user()->id){
            $artist->songs()->detach($song);
        }

    }
        return back();
    }

    public function assoc(Artist $artist, Song $song){

        if($song->destination == 'Music') {

        if(Auth::user()->isAdmin()){
            $artist->songs()->save($song);
        }

        else if(Auth::user()->isModerator() && $song->user->id === Auth::user()->id){
            $artist->songs()->save($song);
        }
    }
        return back();
    }
}
