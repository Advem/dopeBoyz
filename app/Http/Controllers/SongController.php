<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use View;

use App\User;
use App\Song;
use App\Artist;

class SongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($url)
    {
        $url = urldecode($url);
        $artist = preg_replace('/[^A-Za-z0-9]/', ' ', $url);

        $artist = Artist::where('artist_name', $artist)->first();

        if($artist == NULL) return view('layouts.noContentFound');
        else {

        $data['songs'] = DB::table('artist_song') 
        ->join('artists', 'artist_song.artist_id', '=', 'artists.id')
        ->join('songs', 'artist_song.song_id' , '=', 'songs.id') 
        ->where('artists.id', $artist->id)
        ->where('songs.destination', 'Music') 
        ->orderBy('songs.created_at', 'DESC')->get();

        $data['artist'] = $artist;

        $data['artists'] = Artist::orderBy('artists.position', 'ASC')->get();
    
        return view('music.songs', compact('data'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->isAdmin() || Auth::user()->isModerator()){            
            $input = $request->all();
            $defaultCover = '/images/defaultCover/defaultCover.png';

            $title = $request->input('title');
            $destination = $request->input('destination');
            $feature = $request->input('feature');
            $production = $request->input('production');
            $album = $request->input('album');
            $tag = $request->input('tag');
            $description = $request->input('description');
            $embed = $request->input('embed');
            $image = $request->input('image');
            $link_sc = $request->input('link_sc');
            $link_fb = $request->input('link_fb');
            $link_yt = $request->input('link_yt');
            $user_id = Auth::user()->id;

            if ($input['image'] != NULL) {
                Song::create($request->all() 
                + ['user_id' => Auth::user()->id]
                );

                DB::table('backup')->insert(
                    ['text' => "INSERT INTO dopeboyz.songs VALUES(NULL, $user_id, '$destination', '$title', '$feature', '$production', '$album', '$description', '$tag', '$image', '$embed', '$link_fb', '$link_sc', '$link_yt', NULL, NULL);"]
                );
            }

            else {
                Song::create(
                $request->except('image')
                + ['user_id' => Auth::user()->id]
                );

                DB::table('backup')->insert(
                    ['text' => "INSERT INTO dopeboyz.songs VALUES(NULL, $user_id, '$destination', '$title', '$feature', '$production', '$album', '$description', '$tag', '$defaultCover', '$embed', '$link_fb', '$link_sc', '$link_yt', NULL, NULL);"]
                );

                
            }

        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function song($url)
    {
        $url = urldecode($url);
        $url = preg_replace('/[^A-Za-z0-9\Ą\ą\Ć\ć\Ę\ę\Ó\ó\Ś\ś\Ł\ł\Ń\ń\Ż\ż\Ź\ź\-\!]/', ' ', $url);
        // return response($url);
        $song = Song::where('title', $url)->first();

        if($song == NULL) return view('layouts.noContentFound');
        else {
        $data['title'] = $song->title;
        $data['songs'] = Song::where('songs.id', $song->id)
                        ->where('songs.destination', 'Music')
                        ->orderBy('songs.created_at', 'DESC')->get();

        $owner = DB::table('users')->where('id', $song->user_id)->value('id');
        
        $data['artist'] = DB::table('artists')->where('user_id', $owner)->value('artist_name');
        $data['artists'] = Artist::orderBy('artists.position', 'ASC')->get();

        return view('music.song', compact('data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function album($url)
    {
        $url = urldecode($url);
        $album = preg_replace('/[^A-Za-z0-9\Ą\ą\Ć\ć\Ę\ę\Ó\ó\Ś\ś\Ł\ł\Ń\ń\Ż\ż\Ź\ź\-\!]/', ' ', $url);

        $check = Song::where('album', $album)->first();

        if($check == NULL) return view('layouts.noContentFound');
        else {

        $data['title'] = $album;

        $data['songs'] = Song::where('album', $album)
                        ->where('destination', 'Music')
                        ->orderBy('created_at', 'DESC')->get();

        $data['artists'] = Artist::orderBy('artists.position', 'ASC')->get();

        return view('music.album', compact('data'));
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function edit(Song $song)
    {
        if(Auth::user()->isAdmin()){
            return view('song.edit', compact('song'));
        }

        else if(Auth::user()->isModerator()){

            if($song->user->id === Auth::user()->id){
                return view('song.edit', compact('song'));
            }
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function update(Song $song)
    {
        if(Auth::user()->isAdmin() || (Auth::user()->isModerator() && $song->user->id === Auth::user()->id)) {
            $song->title = request('title');
            $song->destination = request('destination');
            $song->feature = request('feature');
            $song->production = request('production');
            $song->album = request('album');
            $song->description = request('description');
            $song->tag = request('tag');
            $song->embed = request('embed');
            $song->link_fb = request('link_fb');
            $song->link_sc = request('link_sc');
            $song->link_yt = request('link_yt');

            if(Auth::user()->isAdmin()) $song->user_id = request('user_id');
            else $song->user_id = Auth::user()->id;
            
            
            if (request('image') == NULL) $song->image = '/images/defaultCover/defaultCover.png';
            else $song->image = request('image');

            $song->created_at = request('created_at');
            $song->save();

            DB::table('backup')->insert(
                ['text' => "UPDATE dopeboyz.songs SET destination = '$song->destination', user_id = '$song->user_id', title = '$song->title', feature = '$song->feature', production = '$song->production', album = '$song->album', description = '$song->description', tag = '$song->tag', image = '$song->image', embed = '$song->embed', link_fb = '$song->link_fb', link_sc = '$song->link_sc', link_yt = '$song->link_yt', created_at = '$song->created_at' WHERE id = $song->id;"]
            );
        }

        return redirect('/admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function destroy(Song $song)
    {
        if(Auth::user()->isAdmin() || Auth::user()->id == $song->user_id) {

            DB::table('artist_song')->where('song_id', '=', $song->id)->delete();

            $song->delete();
        }
        return back();
    }

}
