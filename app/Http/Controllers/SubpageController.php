<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Member;
use App\User;
use App\Subpage;
use App\Song;
use App\Artist;
use View;



class SubpageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Member $member)
    {
        $links = Member::where('members.name', 'dopeBoyz')->get();
        return view('index', compact('links'));
    }

    public function about()
    {
        $data['artists'] = Artist::orderBy('artists.position', 'ASC')->get();
        return view('about.index', compact('data'));
    }

    public function contact(Member $member)
    {
        $links = Member::where('members.name', 'dopeBoyz')->get();
        return view('subpage.contact', compact('links'));
    }

    public function shop(Member $member)
    {
        $links = Member::where('members.name', 'dopeBoyz')->get();
        return view('subpage.shop', compact('links'));
    }

    public function graphics() 
    {
        $data['graphics'] = DB::table('graphics') 
            ->orderBy('graphics.created_at', 'DESC')->get();

        return view('subpage.graphics', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function documentation()
    {
        if (Auth::user()->isAdmin() || Auth::user()->isModerator() || Auth::user()->isDefault()) {
            return view('subpage.documentation');
        }
    
        else return back();
    }

    public function project()
    {
        if (Auth::user()->isAdmin() || Auth::user()->isModerator()) 
        {
            $data['songs'] = Song::where('songs.destination', 'Project')
            ->orderBy('songs.created_at', 'DESC')->get();

            $data['artists'] = Artist::orderBy('artists.position', 'ASC')->get();

            return view('subpage.project', compact('data'));
        }
    
        else return back();
    }

    public function backup()
    {
        if (Auth::user()->isAdmin()) 
        {
            $data['backup'] = DB::table('backup')->orderBy('backup.id', 'ASC')->get();

            return view('subpage.backup', compact('data'));
        }
    
        else return back();
    }

    public function support()
    {
         return view('subpage.support');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subpage  $subpage
     * @return \Illuminate\Http\Response
     */
    public function show(Subpage $subpage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subpage  $subpage
     * @return \Illuminate\Http\Response
     */
    public function edit(Subpage $subpage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subpage  $subpage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subpage $subpage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subpage  $subpage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subpage $subpage)
    {
        //
    }
}
