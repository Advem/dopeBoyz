<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mail\SendMail;

class MailController extends Controller
{
    /**
     *
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */

    public function index() {
        return view('subpage.contact');
    }

    public function sendContactMail(Request $request) {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'message' => $request->message,
        );

        Mail::to('contact@dopeboyz.net')->send(new SendMail($data));
        return back()->with('success', 'Message has been send. Thanks for contacting us!');
    }
}