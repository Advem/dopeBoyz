<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use View;

use App\User;
use App\Artist;
use App\Song;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('user.index');
    }

    public function admin()
    {
        if(Auth::user()->isAdmin()){
            $data['artists'] = Artist::orderBy('position','ASC')->get();
            $data['songs'] = Song::orderBy('created_at','DESC')->get();
        }

        else if(Auth::user()->isModerator()){
            $data['artists'] = Artist::orderBy('position','ASC')->get();
            $data['songs'] = Song::orderBy('created_at','DESC')->get();
            // $data['artist'] = Artist::where('user_id',Auth::user()->id)->get();

            // $song = Song::all()->get();
            // $data['user_ids'] = $song->artist->user_id->get();

            // $data['users'] = Song::with('artist')->get();

        }
        return view('user.admin', compact('data'));
    }

    //Update user data
    public function update(User $user){
        //Checking if the user is editing himself if default
        //If admin user can edit anyone
        if(Auth::user()->id === $user->id || Auth::user()->isAdmin()){
            // $this->validate(request(), [
            //     'firstname' => 'required',
            //     'surname' => 'required',
            //     'address' => 'required',
            //     'city' => 'required',
            //     'postcode' => 'required',
            //     'phone' => 'required',
            //     //'email' => 'required|email|unique:users', for future use
            //     //'password' => 'required|min:6|confirmed'
            // ]);

            if(Auth::user()->isAdmin()) {
                // $user->id = request('id'); // lepiej to wyłączyć !!!
                $user->email = request('email');
                $user->name = request('name');
                if ($user->type != 'Admin') { $user->type = request('type');}
            }

            $user->firstname = request('firstname');
            $user->surname= request('surname');
            $user->address = request('address');
            $user->city = request('city');
            $user->postcode = request('postcode');
            $user->phone = request('phone');

            //$user->password = bcrypt(request('password')); for future use
            $user->save();
        }
        return back();
    }

    public function find($userFind){
        if(Auth::user()->isAdmin()){
            $foundUsers = User::where('name', 'like', '%'.$userFind.'%')
            ->orWhere('email', 'like', '%'.$userFind.'%')
            ->orWhere('type', 'like', '%'.$userFind.'%')
            ->orWhere('id', 'like', '%'.$userFind.'%')
            ->get();
            return $foundUsers->toJson();
        } else {
            return redirect('/');
        }
    }
    public function edit(User $user){
        //return response($user->password);
        if(Auth::user()->isAdmin()){
            return view('user.edit', compact('user'));
        } else {
            return redirect('/');
        }
    }

    public function destroy(User $user){
        if(Auth::user()->isAdmin() && $user->type != 'Admin'){
            $user->delete();
        }
        return redirect('/admin');
    }
}
