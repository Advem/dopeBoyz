<?php
namespace App\Http\Middleware;
use Closure;
class userGroup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        if( auth()->user()->isAdmin() && in_array("Admin", $roles) ) {
            return $next($request); //Keep going if user is an admin
        }
        if(auth()->user()->isModerator() && in_array("Moderator", $roles) ) {
            return $next($request); //Keep going if user is a moderator
        }
        //Redirect unauthorised users, if none rules passed
        return redirect('/user'); 
    }
}