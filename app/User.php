<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    const ADMIN_TYPE = 'Admin';
    const MODERATOR_TYPE = 'Moderator';
    const DEFAULT_TYPE = 'Default';
    use Notifiable;

    //Check if user is an Admin, Moderator or a default user
        public function isAdmin(){
            return $this->type === self::ADMIN_TYPE;
        }
        public function isModerator(){
            return $this->type === self::MODERATOR_TYPE;
        }
        public function isDefault(){
            return $this->type === self::DEFAULT_TYPE;
        }
    //Associate User with Artist
    public function artist(){
        return $this->hasOne('App\Artist');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'firstname', 'surname', 'address', 'city', 'postcode', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    static function GetUserNamebyID($id)
    {
        $name = User::select("name")->where(["id" => $id])->pluck('name');
        if (isset($name[0])) {
            return $name[0];
        } else {
            return '';
        }
    }
}
