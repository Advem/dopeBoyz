<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    protected $fillable = [
        'user_id', 'artist_name', 'position',
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function songs(){
        return $this->belongsToMany(Song::class);
    }
}
