@extends('layouts.master')
@section('title')<title>Documentation - dopeBoyz</title>@endsection
@section('description')<meta name="description" content="dopeBoyz - music group associated with Trap and Hip-Hop genre. Listen to all our music projects and feel the vibes. Join us now!"/>@endsection
@section('content')

    <div class="container align-middle col-sm-12 col-md-10 col-lg-8 col-xl-6 mt-4">
    <h2>Dokumentacja</h2>
        <br><h4>Podstrony</h4>
            <h5 class="text-left ml-5">Home Page</h5>
                    <p class="px-5 text-left">
                    Strona startowa <b>Home Page</b>, to prosta w grafice strona, która ma na celu wyświetlenie użytkownikowi menu <b>[Home Menu]</b> dostępu
                    do innych podstron <b>[Sub Page]</b> takich jak: <i>(About, Music, Graphics, Shop[zostanie ukryte], Contact)</i>, a także po rozwinięciu 
                    dalszej cześci menu, za pomocą znaku białego trójkąta, do podstron <i>(Log in, Registry, Support)</i>. Pasek menu jest responsywny, tj. dostosowuje
                    się do rozmiaru ekranu użytkownika i przy szerokości ekranu <i>576px</i>, pasek menu przybiera drugą formę, gdzie zamiast wylistowanych 
                    przycisków podstron w jednym wierszu, wyświetla się ikona białej strzałki, która po wciśnięciu rozwija pasek menu, którego przyciski ustawione
                    są pionowo. Po wciśnięciu przycisku, użytkownik zostaje przekierowany do danej podstrony i wyświetlony zostanie, na samej górze strony, czarny pasek menu <b>[Main Menu]</b>
                    zawierający logo (którego wciśnięcie spowoduje przekierowanie do <b>Home Page</b>) oraz identyczne przyciski podstron jak na stronie <b>Home Page</b>, 
                    który również jest responsywny zależnie
                    od szerokości ekranu użytkownika i dodatkowo jest zamieszczony na stałe w górnej części ekranu (scrollowanie strony w dół nie spowoduje ukrycia paska <b>Main Menu</b>).
                    Pasek <b>Main Menu</b> wyświetla dodatkową grafikę (biały pasek) pod przyciskiem nazwy aktualnie odwiedzanej podstrony.
                    Na dole strony znajdują się linki do <i>SocialMedia</i>, których adres URL jest przechowywany w bazie danych
                    <i>(tablica: Members)</i> pod nazwą <b>[Member Name]</b> <i>"dopeBoyz"</i>. Linki te są skonstruowane w taki sposób, że
                    po wciśnięciu odpowiedniego przycisku oznaczonego symbolem portalu, przeglądarka użytkownika otworzy
                    nową kartę, nie powodując zamknięcia karty strony <b>dopeBoyz.net</b>. W tle strony startowej widnieje obraz loga, którego
                    zarówno szerokość jak i pozycja również skaluje się wraz ze zmianą wielkości ekranu użytkownika i przyjmuje tendencje 
                    do pozycjonowania na środku ekranu dla ekranów tabletów, komputerów, telewizorów lub pozycjonowania w górnej części dla smartphonów.</p>

            <h5 class="text-left ml-5">About</h5>
                    <p class="px-5 text-left">
                    Podstrona <b>About</b> ma na celu wyświetlenie informacji użytkownikowi oraz zapoznanie go z zespołem, artystami 
                    i innymi osobami tworzącymi grupę <i>dopeBoyz</i>. Treści wyświetlne są w rubrykach, których zawartość pobierana
                    jest z bazy danych <i>(tablica: Members)</i>. Parametry wyświetlane w konkretnej rubryce to:<br> (Nazwa członka <b>[Member Name]</b>, 
                    funkcje jakie pełni w zespole<b> [Member Role]</b> <i>[Chief Officer, Rapper, Producent, Audio Master, Graphic Designer, Redactor, Social Media Manager, Web Developer, itp.]</i>,
                    zwięzły opis członka, linki do social media <i>(Facebook, SoundCloud, Instagram, YouTube, Twitter, SnapChat, itp.)</i>, 
                    liczba wyprodukowanych piosenek [Songs] oraz zdjęcie członka zespołu).<br>
                    Pierwszą z koleji rubryką jest pole poświęcone samemu zespołowi <i>dopeBoyz</i>, jego krótka historia, zarys działalności oraz linki do mediów społecznościowych
                    (dokładnie te same linki jak na stronie <b>Home Page</b>).<br>
                    <b>Update</b>: Aktualnie podstrona About wyświetla statyczny tekst opisu zespołu dopeBoyz oraz nazwy artystów <i>(tablica: Artists)</i> - dynamiczne linki do podstrony artysty, które
                    po wciśnięciu przekierowują użytkownika do strony: <i>dopeBoyz.net/music/ArtistName</i>.</p>

            <h5 class="text-left ml-5">Music</h5>
                    <p class="px-5 text-left">
                    Podstrona <b>Music</b> wyświetla odtwarzacz muzyki najnowszego albumu zespołu, wylistowane piosenki <b>[Songs]</b> wchodzące w skład najnowszego albumu
                    oraz dodatkowy, responsywny pasek menu <b>[Artist Menu]</b>, którego przyciski to nazwy artystów z bazy
                    danych <i>(tablica: Artists)</i>, którego wciśnięcie spowoduje przekierowanie użytkownika do podstrony danego artysty oraz nałożenie dodatkowej 
                    grafiki pod jego nazwą. Treścią podstrony <b>ArtistName</b> są wylistowne utwory muzyczne <b>[Songs]</b>, przypisane do konkretnego artysty.</p>
                    
            <h5 class="text-left ml-5">Graphics</h5>
                    <p class="px-5 text-left">
                    Podstrona <b>Graphics</b> wyświetla wylistowane obrazki w formie kafelków z bazy danych <i>(tablica: Graphics)</i>. Wciśnięcie kafelka obrazu powoduje
                    powiększenie obraz na ekranie oraz wyświetlenie tytułu ilustracji.</p>

            <h5 class="text-left ml-5">Shop</h5>
                    <p class="px-5 text-left">
                    Podstrona <b>Shop</b> wyświetla tekst oraz dynamiczną grafikę strony <i>Facebook</i> zawierającą przycisk <i>Like</i> oraz <i>Share</i> 
                    strony <i>DOPEBOYZFAMILIA</i> na portalu <i>Facebook</i> oraz linki do <i>Social Media</i> w dolnej części storny.</p>

            <h5 class="text-left ml-5">Contact</h5>
                    <p class="px-5 text-left">
                    Podstrona <b>Contact</b> wyświetla formularz kontaktowy, który wysyła wiadomość na skrzynkę pocztową <i>contact@dopeboyz.net</i>.
                    Aby wiadomość została wysłana, użytkownik musi wypełnić poprawnie 3 pola (imię użytkownika, adres email użytkownika, tekst wiadomości).
                    Jeżeli wiadomość została wysłana poprawnie, strona przeładuję się i wyświetli komunikat o powodzeniu. W przeciwnym razie zostanie wyświetlony
                    komunikat o nieprawidłowym wypełnieniu formularza.</p>

            <h5 class="text-left ml-5">Project</h5>
                    <p class="px-5 text-left">
                    Podstrona <b>Project</b>, do której mają dostęp tylko użytkownicy o typie <b>Moderator</b> oraz <b>Admin</b>, wyświetla elemnty <b>Song</b>, 
                    których wartość parametry <b>[Destination]</b> jest równa <i>"Project"</i>.
                    Służy ona do publikacji nowych, niedokończonych utworów muzycznych między członkami zespołu, których pliki źródłowe instenija na serwerze portalu
                    <i>SoundCloud</i> jako niepubliczne, a ich dodanie na podstroną <b>Project</b> umożliwi zespołowi <i>dopeBoyz</i> przesłuchanie ich.</p>

            <h5 class="text-left ml-5">Documentation</h5>
                    <p class="px-5 text-left">
                    Podstrona <b>Documentation</b> zawiera tekstowy opis całego serwisu. Aktualnie dostęp do tej podstrony ma każdy zalogowany użytkownik, nie mniej jedak
                    dostęp do tejże podstrony, będzie ograniczny do użytkowników o typie <b>Moderator</b> oraz  <b>Admin</b> w późniejszym czasie.</p>

        <br><h4>Użytkownicy</h4>
            <h5 class="text-left ml-5">Register</h5>
                    <p class="px-5 text-left">
                    Każdy użytkownik może utworzyć konto za pomocą przycisku <b>Register</b>, które zostanie zapisane w bazie danych <i>(tablica: Users)</i>. 
                    Do utworzenia konta użytkownik musi podać nazwe <b>[User Name]</b>, która, w przypadku moderatorów, będzie widoczna pod piosenką stworzoną przez użytkownika,
                    adres email, który posłuży do logowania się do serwisu, oraz hasła dostępu. 
                    Po stworzeniu nowego konta, użytkownik zostaje przekierowany do formularza profilowego <b>[Profile]</b>.
                    Każdy użytkownik, który utworzył konto, otrzymuje przypisany typ <b>[Default User]</b>, którego dostęp do zaawansowanych funkcjonalności jest ukryty.</p>

            <h5 class="text-left ml-5">Log In</h5>
                    <p class="px-5 text-left">
                    Użytkownik, który posiada konto zapisane w bazie danych, może zalogować się do swojego konta za 
                    pomocą wcześniej podanego loginu i hasła poprzes przycisk <b>Log In</b>. W przypadku nieprawdiłowo
                    wpisanych danych, wyświetlany jest komunikat, a proces logowania kończy się niepowodzeniem. Użytkownik może zresetować swoje hasło wciskając 
                    link w formularzu logowania, który spowoduje przesłanie, na podany adres email wiadomości z instrukcjami do stworzenia nowego hasła.
                    Zarówno pasek <b>Home Menu</b> jak i <b>Main Menu</b>, nie wyświetlają przycisków <i>"Registry"</i> oraz <i>"Log in"</i> w momencie, gdy użytkownik jest zalogowany, natomiast, gdy
                    użytkownik zalogował się do serwisu, na paskach menu pojawiają się przyciski <i>"Profile"</i> oraz <i>"Log Out"</i>.</p>
                
            <h5 class="text-left ml-5">Log Out</h5>
                    <p class="px-5 text-left">
                    Użytkownik posiadający konto, może wylogować się z sesji serwisu za pomocą przycisku <b>Log Out</b>, a jego dane pozostaną ukryte do momentu ponownego zalogowania.</p>

                <h5 class="text-left ml-5">Profile</h5>
                    <p class="px-5 text-left">
                    Podstrona <b>Profile</b> wyświetla użytkownikowi formularz, dzięki którem może on zapisać swoje dane osobiste do bazy danych. Funkcjonalność ta jest przygotowana na wypadek,
                    gdy serwis zaoferuje użytkownikom możliwość zakupu produktów.</p>
                    
            <h5 class="text-left ml-5">Type</h5>
                    <p class="px-5 text-left">
                    Parametr <b>Type</b> użytkownika określa jego poziom autoryzacji. Dostępne są 3 warianty parametru <b>Type</b>: <i>(Default User, Moderator User, Admin User)</i>.</p>
                    <p class="px-5 text-left">
                    Typ <b>Default</b> jest przypisywany każdemu nowemu użytkownikowi, który daje użytkownikowi dostępn do podstrony Profile, gdzie może zaktualizować swoje dane osobowe.
                    <br>
                    Typ <b>Moderator</b> jest przypisany do użytkownika tylko przez użytkownika o typie <b>Admin</b> i umożliwia użytkownikowi ograniczony dostęp do zaawansowanych funkcjonalności i edycji 
                    skonkretyzowanych elementów bazy danych.
                    <br>
                    Typ <b>Admin</b> należy do administracji serwisu i umożliwia użytkownikowi dostęp do wszystkich funkcjonalności. </p>

        <br><h4>Elementy</h4>
                <h5 class="text-left ml-5">Artist</h5>
                    <p class="px-5 text-left">
                    Element <b>Artist</b> to zapis w bazie danych <i>(tablica: Artists)</i>, którego parametry to:<br>
                    nazwa <b>[Artist Name]</b>, powiązanie z użytkownikiem <b>[User ID]</b> oraz powiązanie z piosenką
                    <b>[Song ID]</b>. Do każdego elementu <b>Artist</b> można przyporządkowany wiele elementów <b>Song</b>.
                    Każdy nowo utworzony <b>Artist</b> zostaje wyświetlany na pasku <b>Artist Menu</b>, który widnieje na podstronie <b>Music</b> i nie jest przypisany do żadnego 
                    użytkownika <b>User</b>.<br> Zarówno dodanie nowego elementu <b>Artist</b>
                     jak i przypisanie go do konkretnego użytkownika dokonywane jest przez administracje <b>(Admin User)</b>. Element ten może być powiązany tylko z jednym użytkownikiem.
                    Do każdego elementu <b>Artist</b> może być przyporządkowanych wiele elementów <b>Songs</b>. Wszystkie przypisane utwory muzyczne 
                    <b>Songs</b> danego <b>Artist</b> są wyświetlane na podstronie <b>Music</b> wielkimi literami. Link do podstrony artysty wygląda następująco: 
                    <i>https://dopeBoyz.net/music/<b>ArtistName</b></i>, który może zostać wyświetlony przez dowolnego internaute. Dopuszczalne jest skrócenie linku do postaci:
                    <i>dopeBoyz.net/music/<b>ArtistName</b></i> w celach estetycznych, które może prowadzić do spowolnieniem połączenia z serwerem. Wielkość liter nazwy <b>ArtistName</b>
                    jest nieistotna, nie wpływa na połączenie do podstrony przez link i może być stosowana dowolnie w celach estetycznych.</p>

                <h5 class="text-left ml-5">Song</h5>
                    <p class="px-5 text-left">
                    Element <b>Song</b> to zapis w bazie danych <i>(tablica: Songs)</i>, którego parametry to: tytuł utworu <b>[Title]</b>, lista wokalistów <b>[Feature]</b>, 
                    lista producentów <b>[Production]</b>, tytuł albumu <b>[Album]</b>, opis utworu <b>[Description]</b>, gatunek utworu <b>[Tag]</b>, 
                    link obrazka utworu lub albumu <b>[Image]</b>, link do odtwarzacza <b>[Embed]</b>,
                    linki do utworu na portalach społecznościowych: <b>[LinkFB]</b>, <b>[LinkSC]</b>, <b>[LinkYT]</b>, 
                    powiązanie z elementem <b>[Artist ID]</b>, powiązanie z użytkownikiem <b>[User ID]</b>, data utworzenia utworu <b>[Created At]</b>, data aktualizacji
                    parametrów elementu <b>[Updated At]</b> oraz miejsce docelowe <b>[Destination]</b>. Zarówno dodanie nowego elementu <b>Song</b>
                    jak i przypisanie go do konkretnego elementu <b>Artist</b> dokonywane jest przez użytkowników z poziomem autoryzacji <b>Moderator User</b>.</p>

                    <h6 class="text-left ml-5">Song Destination</h6>
                    <p class="px-5 text-left">
                    Parametr <b>Destination</b> określa miejsce docelowe elementu <b>Song</b> i przyjmuje 3 wartości <i>("Music"</i>, <i>"Project"</i>, <i>"Album")</i>.<br>
                    Wartość <i>"Music"</i> jest domyślną wartością elementu <b>Song</b> podczas dodawania nowego elementu. Tylko te elementy <b>Song</b>, które mają wartość
                    parametru <b>[Destination]</b> równą <i>"Music"</i>, mogą być przypisane do danego artysty i mogą zostać wyświetlane na podstronach <b>Music</b> oraz <b>Artist</b>.<br>
                    Wartość <i>"Project"</i> określa przynależność elementu <b>Song</b> do podstrony <b>Project</b> znajdującej się pod adresem <i>"dopeBoyz.net/project"</i>,
                    która może zostać wyświetlona tylko użytkownikom o typie <b>Moderator</b> oraz <b>Admin</b>. Elementy o wartości <i>"Project"</i> nie mogą zostać przypisane
                    do żadnego artysty. <br>
                    Wartość <i>"Album"</i> określa element <b>Song</b> jako element do wyświetlenia na podstronie <b>Music</b> jako rozbudowany player całego 
                    albumu za pomocą linku parametru <b>[Embed]</b>.
                    W bazie powinien znajdować się tylko jeden taki element, który będzie wyświetlany jako "Najnowszy Album". Wszystkie elementu <b>Song</b>, których parametr
                    <b>[Album]</b> jest taki sam jak parametr <b>[Title]</b> elementu o wartości parametrzu <b>[Destination]</b> równą <i>"Album"</i> zostaną wyświetlone na podstronie <b>Music</b>.
                    W celu aktualizacji najnowszego albumu należy edytować istniejący element <b>Album</b> lub stworzyć nowy, usuwając poprzedni.
                    W przypadku, gdy w bazie danych nie istnieje żaden element <b>Album</b>, podstrona <b>Music</b> nie zostanie załadowana.</p>
                    
                    <h6 class="text-left ml-5">Song Title</h6>
                    <p class="px-5 text-left">
                    Parametr <b>Title</b> to tytuł utworu, który powinien się składać z maksymalnie <i>30</i> znaków oddzielanych spacjami, gdyż w przeciwnym wypadku pozostałe parametry
                    zaburzą estetyczny układ elementu. Parametr ten również powinien się składać z maksymalnie jednego wyrazu posiadającego <i>12</i> symboli, gdyż w przeciwnym wypadku
                    tytuł nie zostanie wyświetlony w pełnej postaci na urządzeniach o szerokości ekranu <i>360px</i>. 
                    Tytuł, ze względów estetycznych, powinien składać się z wyrazów zapoczątkowanych
                    wielką literą (<i>"Song Title Looks Nice Now"</i> ). Polskie znaki na ten moment nie są akceptowalne z powodu błędnego odczytu adresów URL.</p>

                    <h6 class="text-left ml-5">Song Feature</h6>
                    <p class="px-5 text-left">
                    Parametr <b>Feature</b> zawiera informację o wokalistach. Ze względów estetycznych treść 
                    powinna zawierać nazwy wokalistów zapoczątkowanych wielką literą, oddzielonych małą literą
                    <i>"<b>x</b>"</i> (<i>"Artist1 <b>x</b> Artist2 <b>x</b> Artist3"</i> ).</p>

                    <h6 class="text-left ml-5">Song Production</h6>
                    <p class="px-5 text-left">
                    Parametr <b>Production</b> zawiera informację o twórcach podkładu muzycznego. 
                    Ze względów estetycznych treść powinna zawierać nazwy producentów zapoczątkowanych wielką literą, oddzielonych małą literą
                    <i>"<b>x</b>"</i> (<i>"Producent1 <b>x</b> Producent2 <b>x</b> Producent3"</i> ).<br> Obydwa powyższe parametry wyświetlane są w układzie
                    elementu <b>Song</b> w jednej linii i oddzielone są od siebie wyrazem <i>"prod."</i> w celu rozróżnienia
                    twórców. (<i>"Artist1 <b>x</b> Artist2 <b>prod.</b> Producent1 <b>x</b> Producent2"</i> ).</p>

                    <h6 class="text-left ml-5">Song Album</h6>
                    <p class="px-5 text-left">
                    Parametr <b>Album</b> służy jedynie do grupowania piosenek i nie jest wyświetlany w układzie elementu <b>Song</b>. 
                    Jeżeli element <b>Song</b> posiada wartość parametru <b>Album</b>, która jest taka sama jak tytuł <b>[Title]</b> elementu, którego parametr <b>[Destination]</b> ma wartość <i>"Album"</i> 
                    zostanie on wyświetlony na podstronie <b>Music</b> jako składowa "najnowszego albumu". Polskie znaki na ten moment nie są akceptowalne z powodu błędnego odczytu adresów URL.</p>

                    <h6 class="text-left ml-5">Song Tag</h6>
                    <p class="px-5 text-left">
                    Parametr <b>Tag</b> określa gatunek muzyczny utworu. Treść parametru powinna składać się z maksymalnie <i>11</i> symboli zapoczątokowana wielką literą. 
                    W przeciwnym wypadku treść parametru zostanie wysunięta po za obramowanie, powodując nieład układu elementu.
                    Pusta wartość parametru <b>[Tag]</b> spowduje wyświetlenie pustej grafiki w układzie elementu <b>Song</b>.</p>

                    <h6 class="text-left ml-5">Song Description</h6>
                    <p class="px-5 text-left">
                    Parametr <b>Description</b> zawiera infromację opisującą utwór muzyczny. Treść opisu nie powinna przekroczyć <i>255</i> znaków. 
                    Opis wyświetlany na ekranach o szerokości <i>360px</i>
                    zostanie skrócony do <i>80</i> znaków w celu zachowania układu elementu. 
                    Wraz ze zwiększaniem szerokości ekranu limit znaków będzie zachowywał tendencję rosnącą aż do momentu,
                    w którym zostanie wyświetlona cała treść opisu.</p>

                    <h6 class="text-left ml-5">Song Image</h6>
                    <p class="px-5 text-left">
                    Parametr <b>Image</b> to adres linku obrazu, który zostanie wyświetlony w układzie elementu jako kwadrat o wymiarach <i>150x150 pikseli</i>. 
                    Kliknięcie w obraz elementu <b>Song</b> spowoduje uruchomienie odtwarzacza <b>Player</b>, który jest bezpośrednio połączony z parametrem <b>[Embed]</b>. 
                    Adres obrazu może być dowolny, pod 
                    warunkiem, że plik obrazu jest dostępny globalnie. Własne grafiki należy umieścić 
                    na portalach społecznościowych lub na dysku wirtualnym w trybie publicznym. Uzyskanie adresów grafik z 
                    serwisów internetowych można wykonać poprzez odnalezienie ukrytego adresu za pomocą silnika przeglądarki:<br> (używając <i>Google Chrome</i>)
                    należy uruchomić tryb programisty skrótem 
                    klawiszowym <i>Ctrl+Shift+I</i> lub kliknąć prawym przyciskiem myszy na obraz i wybierając opcje <i>Zbadaj</i>, następnie przejść do zakładki Elementy/Elements i uruchomić,
                    narzędzie podglądania za pomocą 
                    skrótu klawiszowego <i>Ctrl+Shift+C</i> lub pierwszego przycisku na pasku zakładek i wciśnięcie lewym przyciskiem myszy porządany obraz.
                    Zostanie wówczas podświetlona linia kodu z adresem linku.<br> Należy skopiować 
                    zawartość cudzysłowa funkcji <i>url("<b>SongImage</b>");</i>  i przekopiować ją do formularza.
                    Przykładowo podświetlony zostanie kod:<br> <i>span style="background-image: 
                    url("<b>https://i1.sndcdn.com/artworks-000452494287-txctvz-t500x500.jpg</b>"); width: 100%; height: 100%; opacity: 1; ..."</i><br>
                    Adresem grafiki jest link wyróżniony pogrubionym tekstem.</p>

                    <h6 class="text-left ml-5">Song Embed</h6>
                    <p class="px-5 text-left">
                    Parametr <b>Embed</b> służy do łączenia elementu <b>Song</b> z plikiem utworu muzycznego znajdującego 
                    się na serwerze <i>SoundCloud</i>. Aby użytkownik mógł uruchomić <b>Player</b> w celu 
                    przesłuchania pliku muzycznego danego elementu należy do wartości Parametru <b>[Embed]</b> przypisać 
                    wartość jaką jest adres linku. Uzyskanie adresu z serwisu <i>SoundCloud</i> wygląda
                    następująco:<br> Należy odnaleźć utwór muzyczny na serwisie i otworzyć jego pełną stronę, 
                    której adres przykładowo wygląda w ten sposób:<br> <i>https://soundcloud.com/xannytrejoofroswell/memento-mori</i><br> 
                    Następnie należy przycisnąć przycisk (<i>Udostępnij </i>), wybrać opcję (<i>Umieść na stronie </i>), 
                    zaznaczyć środkowy styl odtwarzacza (<i>Classic embed </i>), wybrać kolor odtwarzacza, który pasuje do 
                    grafiki elementu, odznaczyć opcję (<i>Włącz automatyczne odtwarzanie </i>) i przekopiować adres z pola (<i>Kod </i>). 
                    Następnie należy skopiować tylko zawartość funkcji <i>src="<b>SongEmbed</b>"</i>
                    (zaznaczone wygrubionym tekstem; spacje zostały umieszczone celowo)
                    przykładowo:<br> <i><\iframe width="100%" height="166" scrolling="no" frameborder="no" 
                    allow="autoplay" src="<b>https:// w.soundcloud.com/ player/ ?url=https%3A// api.sound
                    cloud.com/ tracks/539263578& color=%232c2c14& auto_play=false& hide_related=false& 
                    show_comments=true& show_user=true& show_reposts=false& show_teaser=true</b>"><\/iframe></i><br>
                    a następnie wkleić adres do pola formularza parametru <b>[Embed]</b>. <b>Player</b> aktywowany jest poprzez wciśnięcie obrazka elementu <b>[Song Image]</b>.</p>

                    <h6 class="text-left ml-5">Song Links</h6>
                    <p class="px-5 text-left">
                    Paramtery <b>Links</b> <i>SocialMedia</i> służą do łączenia elementu <b>Song</b> z portalami społecznościowymi za pomocą hiperłączy. 
                    Treścią parametru jest adres URL pozyskiwany
                    z adresu platformy. Links są wyświetlane w układzie elementu <b>Song</b> za pomocą ikon - konkretny parametry jest oznakowany logiem portalu społecznościowego, a 
                    wciśnięcie go przez użytkownika przekieruje go na stronę docelową, otwierając nową kartę w przeglądarce, nie powodując zamknięcia karty strony <i>dopeBoyz.net</i>.
                    W przypadku, gdy Paramtery <b>[Links]</b> jest pusty tj. jego wartość nie posiada treści, nie zostanie on wyświetlony w układzie <b>Song</b>.</p>

                    <h6 class="text-left ml-5">Song Buttons</h6>
                    <p class="px-5 text-left mb-0">
                    W skład elementu <b>Song</b> wchodzą również dodatkowe przyciski, które znajdują się tuż obok <b>Song Links</b>:
                    <ul class="text-left list-group pl-5 text-black-75 py-0 my-0">
                            <li class="nobull">Ikona Nuty - przycisk przekierowujący użytkownika do podstrony, na której znajduje się tylko dany element Song 
                            pod adresem: <i>https://dopeBoyz.net/song/SongTitle</i></li>
                            <li class="nobull">Ikona Płyty - przycisk przekierowujący użytkownika do podstrony, na której znajdują się wszystkie elementy Song, które 
                            posiadają tą samą wartość parametru <b>[Album]</b> pod adresem: <i>https://dopeBoyz.net/album/SongAlbum</i></li>
                            <li class="nobull">Ikona Edycji - przycisk przekierowujący do panelu edycji elementu Song, wyświetlana tylko, gdy użytkownik jest właścicielem tego elementu.</li>
                            <li class="nobull">Ikona Kosza - przycisk usuwający elementu Song, wyświetlana tylko, gdy użytkownik jest właścicielem tego elementu.</li>
                        </ul></p>
                    </p>

                    <h6 class="text-left ml-5">Song Created At</h6>
                    <p class="px-5 text-left">
                    Parametr <b>Created At</b> służy do przechowywania dokładnego czasu powstania elementu <b>Song</b>, dzięki czemu, w układzie <b>Song</b> generowany jest wpis
                    wyświetlający ilość czasu, jaka upłyneła od daty powstania elementu. Wyświetlanie czasu wygląda następująco:<br>
                    jeżeli od powstania elementu upłynął czas mniejszy niż jedna minuta - wyświetlona zostanie ilość sekund oraz napis "<i>sec ago</i>";<br>
                    jeżeli od powstania elementu upłynął czas mniejszy niż jedna godzina - wyświetlona zostanie ilość minut oraz napis "<i>mins ago</i>";<br>
                    jeżeli od powstania elementu upłynął czas mniejszy niż jeden dzień - wyświetlona zostanie ilość godzin oraz napis "<i>hours ago</i>";<br>
                    jeżeli od powstania elementu upłynął czas mniejszy niż jeden miesiąc - wyświetlona zostanie ilość dni oraz napis "<i>days ago</i>";<br>
                    jeżeli od powstania elementu upłynął czas mniejszy niż jeden rok - wyświetlona zostanie ilość miesięcy oraz napis "<i>months ago</i>";<br>
                    jeżeli od powstania elementu upłynął czas większy lub równy jeden rok - wyświetlona zostanie ilość lat oraz napis "<i>years ago</i>";<br>
                    W przypadku, gdy ilość czas jak upłynęła od czasu stworzenia elementu jest równa <i>1</i> tzn. "<i>jeden miesiąc</i>",<br> wyświetlony zostanie napis "<i>month ago</i>"
                    zamiast napisu "<i>months ago</i>" oraz analogicznie dla pozostałych przypadków.<br> 
                    Parametr <b>[Created At]</b> jest generowany automatycznie w momencie dodawnia nowego elementu <b>Song</b> do bazy danych <i>(tablica: Songs)</i>, nie mniej jednak,
                    w celu usprawnienia bazy danych, w których utwory muzyczne artystów zostały stworzone o wiele wcześniej, dodano możliwość zmiany wartości paramteru 
                    <b>[Created At]</b> na dowolną w formacie RRRR-MM-DD hh:mm:ss "<i>1997-09-13 03:14:27 </i>", którą można ustawić w panelu aktualizacji elementu <b>Song</b>.</p>

                    <h6 class="text-left ml-5">Song Fresh</h6>
                    <p class="px-5 text-left">
                    W przypadku, gdy wartość parametru <b>[Created_At]</b> wynosi <i>mniej niż 7 dni</i> w układzie elementu <b>Song</b> zostaniej wyświetlona dodatkowa grafika z napisem <i>Fresh</i>.</p>

                    <h6 class="text-left ml-5">Song Updated At</h6>
                    <p class="px-5 text-left">
                    Parametr <b>Updated At</b> służy do przechowywania dokładnego czasu jaki upłynął od ostatniej edycji 
                    elementu <b>Song</b>, dzięki czemu, administracja seriwus otrzymuje informację o zmianie parametrów elementu.
                    Parametr <b>[Updated At]</b> jest generowany automatycznie w momencie aktualizacji istniejącego elementu <b>Song</b> w bazie danych <i>(tablica: Songs)</i>.</p>

                    <h6 class="text-left ml-5">Song Artist ID</h6>
                    <p class="px-5 text-left">
                    Parametr <b>Artist ID</b> służy do przechowywania informacji, który element <b>Artist</b> jest powiązany z 
                    elementem <b>Song</b>, dzięki czemu dany element <b>Song</b> zostanie wyświetlony w liście elementów <b>Songs</b> danego
                    elementu <b>Artist</b> na podstronie <b>Music</b> konkretnego artysty ("<i>dopeBoyz.net/music/<b>ArtistName</b></i> "). 
                    Treścią tego parametru jest dowolny, istniejący w bazie danych <i>(tablica: Artist)</i> element <b>Artist</b>. Każdy 
                    element <b>Song</b> może być przyporządkowany wielu elementom <b>Artist</b>. Przyporządkowanie konkretnemu 
                    elementowi <b>Song</b> treści parametru <b>[Artist ID]</b> jest dokonywane poprzez panel <b>Artist Songs</b> konkretnego elementu <b>Artist</b>.</p>

                    <h6 class="text-left ml-5">Song User ID</h6>
                    <p class="px-5 text-left">
                    Parametr <b>User ID</b> służy do przechowywania informacji, który użytkownik <b>User</b> jest powiązany z 
                    elementem <b>Song</b>, dzięki czemu dany element <b>Song</b> może zostać edytowany, aktualizowany tylko przez konkretnego użytkownika <b>User</b>.
                    Treścią tego parametru jest tylko konkretny, istniejący w bazie danych <i>(tablica: Users)</i> użytkownik <b>User</b>. Każdy 
                    element <b>Song</b> może być przyporządkowany tylko jednemu użytkownikowi <b>User</b>. 
                    Przyporządkowanie konkretnemu elementowi <b>Song</b> treści parametru <b>[User ID]</b> jest dokonywane automatycznie podczas dodawania nowego elementu <b>Song</b>
                    do bazy danych <i>(tablica: Songs)</i> przez aktualnie zalogowanego użytkownika <b>User</b>, którego typ autoryzacji ma okreslony priorytet jako <b>Moderator User</b>.
                    Jeżeli przeczytałeś to wszystko powyżej, dokończe resztę, zarejestruj się i napisz do mnie na <i>Messanger</i> wiadomość "Chyba oszalałeś z tą dokumentacją"
                    , wówczas dam Ci prawa Moderatora. Nie mów reszcie tajnego hasła - niech przeczytają wszyscy, żeby każdy wiedział jak to działa.</p>
    
            <br><h4>Moderyzacja</h4>
                    <h6 class="text-left ml-5">Panel Moderatora</h6>
                    <p class="px-5 text-left mb-0">
                    W Panelu Moderatora znajdują się:<br> 
                    1. lista elementów <b>Artist</b>, w której skład wchodzi:
                        <ul class="text-left list-group pl-5 text-black-75 py-0 my-0">
                            <li class="nobull">Nazwa Artist</li>
                            <li class="nobull">Nazwa User, który jest właścicielem elementu Artist</li>
                            <li class="nobull">Liczba posiadanych elementów Song</li>
                            <li class="nobull">Przycisk edycji elementu Artist (gdy User jest właśicielem tego elementu)</li>
                            <li class="nobull">Przycisk przekierowujący do podstorny artysty <i>dopeBoyz.net/music/ArtistName</i></li>
                            <li class="nobull">Przycisk przekierowujący do Panelu Artysty</li>
                        </ul></p>
                    <p class="px-5 text-left mb-0">
                    2. Formularz dodawania nowych elementów <b>Song</b><br>
                    3. Lista elementów <b>Song</b>, których tło zależne jest od właściciela elementu, określone kolorem:
                        <ul class="text-left list-group pl-5 text-black-75 py-0 my-0">
                            <li class="nobull">Niebieski - User jest właścicielem elementu</li>
                            <li class="nobull">Czerwony - User nie jest właścicielem elementu</li>
                            <li class="nobull">Biały - Element jest traktowanych jako <b>Album</b>, wyświetlany na podstronie <b>Music</b> <i>jako "Najnowszy Album"</i></li>
                        </ul>
                    </p>
                    <h6 class="text-left ml-5">Artist</h6>
                    <p class="px-5 text-left">
                    Administrator może dodać nowego artystę do bazy danych, który później połączony zostanie z jednym użytkownikiem o typie <b>Moderator</b>, który staję się właścielem 
                    danego elementu <b>Artist</b>. Może on wówczas edytować nazwę oraz odłączać elementy <b>Song</b> od swojego elementu <b>Artist</b>.
                    </p>

                    <h6 class="text-left ml-5">Panel Artist</h6>
                    <p class="px-5 text-left">
                    Panel Artist danego elementu <b>Artist</b> składa się z dwóch części: listy aktualnie przypisanych elementów <b>Song</b> oraz listy elementów <b>Song</b>, 
                    których <b>User</b> jest właścicielem. Użytkownik, który jest właścicielem elementu <b>Artist</b> może odłączyć wszystkie elementy Song ze swojego Artist.
                    Użytkownik, który nie jest właścicielem elementu <b>Artist</b> może odłączyć tylko te elementy Song, których jest właścicielem.
                    Użytkownik może dołączać do każdego elementu Artist elementy Song, których jest właścicielem, ale tylko te, które posiadają parametr <b>[Destination]</b> równy <i>"Music"</i>. 
                    Dołączanie i odłączanie elementów Song to elementów Artist wykonuje się za pomocą przycisków po prawej stonie elementów Song. W momencie, gdy użytkownik nie ma uprawnień do wykonania danej operacji, przyciski
                    te nie zostaną wyświetlone.
                    </p>

                    <h6 class="text-left ml-5">Song</h6>
                    <p class="px-5 text-left">
                    Użytkownik, może edytować i usuwać tylko te elementy <b>Song</b>, których jest właścicielem. 
                    </p>
    
    </div>
@endsection