@extends('layouts.master')
@section('title')<title>Contact - dopeBoyz</title>@endsection
@section('description')<meta name="description" content="Chat with dopeBoyz Crew. Whether you have a question about our projects, collaboration, production send us an Email now!"/>@endsection
@section('content')

    @include('layouts.socialLinks')

    <img src="/images/logo/dopeBoyzLogo2019sign.png" class="img-responive d-block mx-auto" id="about-img-sign" alt="dopeBoyz Sign">
    <div class="container align-middle about-container justify-content-center col-10 col-sm-10 col-md-8 col-lg-7 col-xl-6 px-2 anim-song">
        <h1 class="contact-h1 pb-2 pt-5 text-center">Contact Us</h1>
        <h2 class="my-0 mx-auto pt-1 pb-3 contact-h2 text-center col-xs-12 col-10">Feel free to chat with dopeBoyz!</h2>
        <h3 class="my-0 mx-auto contact-h3 text-center col-xs-12 col-sm-10 col-xl-8">Whether you have a question about our projects, 
        collaboration, production, or anything else, our team is ready to answer all of it (English/Polish/Russian)</h3>
        
        @if(count($errors) > 0)
            <div class="row justify-content-center w-100 mx-auto col-xs-12 col-sm-10 col-xl-8 contact-alert mt-4">
                @foreach($errors->all() as $error)
                    <strong class="mx-auto text-danger">{{ $error }}</strong>
                @endforeach
            </div>
        @endif

        @if($message = Session::get('success'))
            <div class="row justify-content-center w-100 mx-auto col-xs-12 col-sm-10 col-xl-8 contact-success mt-4">
                <strong class="mx-auto text-success">{{ $message }}</strong>
            </div>
        @endif

        <div class="row justify-content-center w-100 mx-0 px-0 pt-4 pb-5 mb-3">
            <form method="POST" action="/contact/send" class="mb-3 justify-content-md-end row px-0 col-12 col-xl-8">
                {{ csrf_field() }}
                {{ method_field('put') }}

                <div class="input-group pb-3 px-0 col-12">
                    <input name="name" type="text" class="form-control contact-form" placeholder="Your Name">
                </div>

                <div class="input-group pb-3 px-0 col-12">
                    <input name="email" type="email" class="form-control contact-form" placeholder="Your Email">
                </div>

                <div class="input-group pb-3 px-0 col-12">
                    <textarea name="message" type="text" class="form-control contact-form" rows="5" placeholder="Message"></textarea>
                </div>

                <div class="row w-100 justify-content-center mx-0 px-0">
                    <button name="send" type="submit" class="btn btn-dark contact-button col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection