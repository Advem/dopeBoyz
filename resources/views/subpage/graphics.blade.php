@extends('layouts.master')
@section('title')<title>Graphics - dopeBoyz</title>@endsection
@section('description')<meta name="description" content="Visit dopeBoyz Graphics page to see our album covers, logos, designs and more..."/>@endsection
@section('content')

<img src="/images/logo/dopeBoyzLogo2019sign.png" class="img-responive d-block mx-auto dopeboyz-sign z-index-00" alt="dopeBoyz Sign">
    <div class="row justify-content-center w-100 mx-auto col-xs-12 col-12 col-xl-9 graphics-field py-3 px-0">
        @foreach($data['graphics'] as $graphic)
        <div class="d-flex flex-wrap py-3 px-3 graphics-square">
            <img alt="{{ $graphic->title }}" title="{{ $graphic->title }}" src="{{ $graphic->image }}" class="img-fluid graphics-img">
        </div>
        @endforeach
        <div class="d-flex flex-wrap py-3 px-3 graphics-square"><img alt="Tha Golden Triangle Cover" title="Tha Golden Triangle Cover" src="https://i1.sndcdn.com/artworks-000522556812-b4dm0f-t500x500.jpg" class="img-fluid graphics-img"></div>
        <div class="d-flex flex-wrap py-3 px-3 graphics-square"><img alt="Tha Golden Triangle Cover" title="Tha Golden Triangle Cover" src="https://i1.sndcdn.com/artworks-000362111328-vqlzuh-t500x500.jpg" class="img-fluid graphics-img"></div>
        <div class="d-flex flex-wrap py-3 px-3 graphics-square"><img alt="Tha Golden Triangle Cover" title="Tha Golden Triangle Cover" src="https://i1.sndcdn.com/artworks-000440393700-051ujh-t500x500.jpg" class="img-fluid graphics-img"></div>
        <div class="d-flex flex-wrap py-3 px-3 graphics-square"><img alt="Tha Golden Triangle Cover" title="Tha Golden Triangle Cover" src="https://i1.sndcdn.com/artworks-000407259138-obg3uk-t500x500.jpg" class="img-fluid graphics-img"></div>
        <div class="d-flex flex-wrap py-3 px-3 graphics-square"><img alt="Tha Golden Triangle Cover" title="Tha Golden Triangle Cover" src="https://i1.sndcdn.com/artworks-000462027066-o6i1md-t500x500.jpg" class="img-fluid graphics-img"></div>
    </div>

@endsection