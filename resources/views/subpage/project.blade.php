@extends('layouts.master')
@section('title')<title>dopeBoyz - Project</title>@endsection
@section('description')<meta name="description" content="dopeBoyz - music group associated with Trap and Hip-Hop genre. Listen to all our music projects and feel the vibes. Join us now!"/>@endsection
@section('content')
<div class="mt-4"></div>
    @include('layouts.songsList')
    <div class="row w-100 justify-content-center mx-0 mt-2 anim-home-menu">
        <div class="button-dark"><a href="/admin">Back</a></div>
    </div>

@endsection