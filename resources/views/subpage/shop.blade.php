@extends('layouts.master')
@section('title')<title>Shop - dopeBoyz</title>@endsection
@section('description')<meta name="description" content="dopeBoyz Shop will be launched soon. Visit our shop for more details."/>@endsection


@section('content')
    @include('layouts.socialLinks')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v3.2"></script>

    <img src="/images/logo/dopeBoyzLogo2019sign.png" class="img-responive d-block mx-auto" id="about-img-sign" alt="dopeBoyz Sign">
    <div class="container h-100 my-auto align-middle col-10 col-sm-10 col-md-8 col-lg-7 col-xl-5 px-2 anim-song pt-shop">
        <h1 class="text-white p-0">Shop</h1>
        <h2 class="about-text-content pt-2 pb-4">Will be launched after reaching <b>5000 Likes</b> on our Facebook page!</h2>
        <div class="fb-like" data-href="https://www.facebook.com/dopeboyzFAMILIA/" data-width="" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
    </div>
@endsection