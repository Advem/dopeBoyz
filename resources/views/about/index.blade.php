@extends('layouts.master')
@section('title')<title>About - dopeBoyz</title>@endsection
@section('description')<meta name="description" content="Read about dopeBoyz group and listen to our music now!"/>@endsection
@section('content')

    <img src="/images/logo/dopeBoyzLogo2019sign.png" class="img-responive d-block mx-auto" id="about-img-sign" alt="dopeBoyz Sign" title="dopeBoyz">
    <h1><a href="/"><img src="/images/logo/dopeBoyzLogo2019title.png" class="img-responive d-block mx-auto anim-top-nav" id="about-img-title" alt="dopeBoyz Logo" title="dopeBoyz"></a></h1>
 
    <div class="container align-middle about-container col-10 col-sm-10 col-md-8 col-lg-7 col-xl-5 px-2 anim-song">
        <h2 class="about-text-head text-left">What is dopeBoyz?</h2>
        <p class="my-0 about-text-content text-left">dopeBoyz is a music group from Poland created in 2017 assocciated with Trap and Hip-Hop genre.
            The main goal of the band is to share the work of artists with an audience that has similar musical taste. We post
            our projects on music platform - SoundCloud.</p>
        <h2 class="pt-5 about-text-head text-left">Who is dopeBoyz?</h2>
        <p class="my-0 about-text-content text-left">Currently our crew counts 11 members including 7 artists, 
            producers, beat-makers, vocalists as well as graphic designer, web developer, marketing and social media managers.</p>
        <h2 class="pt-5 about-text-head text-left">Music Production</h2>
        <p class="my-0 about-text-content text-left">Music is our passion and we focus on delivering the best quality content to our 
            listeners. To do that we compose music in software called Fruti Loop Studio which is our daily workplace.</p>
        <h2 class="pt-5 about-text-head text-left">Artists</h2>
        <p class="my-0 about-text-content text-left">Mainly we create larger themed projects that bring all artists together although each of us creates 
            their own contet saparetely.  You can listen to those tracks by picking an artist on music subpage
            or by clicking name below.</p>
    </div>

          <div class="d-flex flex-wrap justify-content-center mb-0 py-5 about-artist-names">
            @foreach ($data['artists'] as $artist)
                <a class="px-5 py-5 about-artist anim-home-menu" href="/music/{{ urlencode($artist->artist_name) }}" title="Listen to {{ $artist->artist_name }}'s Tracks">{{ $artist->artist_name }}</a>
            @endforeach
        </div>

@endsection
