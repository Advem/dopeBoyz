@extends('layouts.master')
@section('title')<title>{{$song->title}} - Song Edit - dopeBoyz</title>@endsection
@section('description')<meta name="description" content="dopeBoyz - music group associated with Trap and Hip-Hop genre. Listen to all our music projects and feel the vibes. Join us now!"/>@endsection

@section('content')
    <div class="container mt-4">
        <div class="row justify-content-center w-100 ml-0">
                <form method="POST" action=" {{ route('song.update', $song) }} " class="col-md-8 mb-3 justify-content-md-end row">
                    {{ csrf_field() }}
                    {{ method_field('patch') }}
                <div class="input-group mb-3 col-md-12">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Song</span>
                    </div>
                    <input name="title" type="text" class="form-control" value="{{$song->title}}">
                </div>

                <div class="input-group mb-3 col-md-12">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Destination</span>
                    </div>
                    <select name="destination" type="text" class="form-control pl-2">
                        <option value="{{$song->destination}}" selected hidden>{{$song->destination}}</option>
                        <option value="Music">Music</option>
                        <option value="Album">Album</option>
                        <option value="Project">Project</option>
                    </select>
                </div>

                <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Feature</span>
                        </div>
                        <input name="feature" type="text" class="form-control" value="{{$song->feature}}">
                </div>

                <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Prodction</span>
                        </div>
                        <input name="production" type="text" class="form-control" value="{{$song->production}}">
                </div>

                <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Album</span>
                        </div>
                        <input name="album" type="text" class="form-control" value="{{$song->album}}">
                </div>

                <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Tag</span>
                        </div>
                        <input name="tag" type="text" class="form-control" value="{{$song->tag}}">
                </div>

                <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Description</span>
                        </div>
                        <input name="description" type="text" class="form-control" value="{{$song->description}}">
                </div>

                <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Embed</span>
                        </div>
                        <input name="embed" type="text" class="form-control" value="{{$song->embed}}">
                </div>

                <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Image</span>
                        </div>
                        <input name="image" type="text" class="form-control" value="{{$song->image}}">
                </div>

                <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Link<i class="fab fa-soundcloud text-muted pl-3"></i></span>
                        </div>
                        <input name="link_sc" type="text" class="form-control" value="{{$song->link_sc}}">
                </div>

                <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Link<i class="fab fa-facebook text-muted pl-3"></i></span>
                        </div>
                        <input name="link_fb" type="text" class="form-control" value="{{$song->link_fb}}">
                </div>

                <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Link<i class="fab fa-youtube text-muted pl-3"></i></span>
                        </div>
                        <input name="link_yt" type="text" class="form-control" value="{{$song->link_yt}}">
                </div>

                <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Created</span>
                        </div>
                        <input name="created_at" type="text" class="form-control" value="{{$song->created_at}}">
                </div>

                @auth @if(Auth::user()->isAdmin())
                <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">{{$song->user->name}}</span>
                        </div>
                        <input name="user_id" type="text" class="form-control" value="{{$song->user_id}}">
                </div>
                @endif @endauth
                    
                <div class="row w-100 justify-content-center mx-0">
                    <button type="submit" class="button-dark">Update</button>
                    <div class="button-dark"><a href="/admin">Back</a></div>
                </div>
        
                </form>
            </div>
    </div>
@endsection