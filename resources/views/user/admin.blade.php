@extends('layouts.master')
    @if (Auth::user()->isAdmin())
        @section('title')
            <title>Admin Panel - dopeBoyz</title>
        @endsection
    @endif
    @if (Auth::user()->isModerator())
    @section('title')
            <title>Mod Panel - dopeBoyz</title>
        @endsection
    @endif
    @section('description')<meta name="description" content="dopeBoyz - music group associated with Trap and Hip-Hop genre. Listen to all our music projects and feel the vibes. Join us now!"/>@endsection
    <!-- @section('script')<script src="/js/bootstrap.js"></script>@endsection -->
@section('content')
    @include('layouts.functions')
    @if (Route::has('login'))
    @auth

    <div class="container w-100 mt-4">    

  
        <div class="row justify-content-center w-100 mx-0">
        @if (Auth::user()->isAdmin()) <h2 class="w-100 mx-0 px-0">Admin Panel</h2> @endif
        @if (Auth::user()->isModerator()) <h2 class="w-100 mx-0 px-0">Moderator Panel</h2> @endif
        </div>

    @if (Auth::user()->isAdmin())
        <div class="row justify-content-center mb-0 w-100 mx-0">
            <div class="input-group mb-0 col-md-10 col-lg-7 w-100 mx-0">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">User</span>
                </div>
                <input id="userSearch" type="text" class="form-control" placeholder="Type in User Name or Email">
            </div>
        
            <ul class="list-group mt-2 col-md-10 col-lg-7 users-list justify-content-center px-3">
            </ul>
        </div>
    @endif

    @if (Auth::user()->isAdmin())
        <div class="row justify-content-center mt-2 mb-3 w-100 mx-0">
            <form method="POST" action="/artist/add" class="justify-content-center col-md-10 col-lg-7">
                {{ csrf_field() }}
                {{ method_field('put') }}
                <div class="input-group mb-3 justify-content-center w-100 mx-0">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Artist</span>
                    </div>
                    <input name="artist_name" type="text" class="form-control" placeholder="Type in Artist Name to Add">
                </div>
            </form>
            <!-- <div class="row w-100 justify-content-center mx-0 px-3">
            <button type="submit" class="btn btn-dark col-xs-10 col-sm-12 col-md-5 col-lg-2 col-xl-1">Add</button>
                </div> -->
        </div>
    @endif
        <!-- //////////////////////////// -->
        <div class="row justify-content-center w-100 mx-0 mb-3">
            <ul class="list-group col-xs-12 col-sm-12 col-md-10 col-lg-7 col-xl-7 justify-content-center px-3">
                @foreach ($data['artists'] as $artist)
                    <li class="list-group-item w-100 mx-0 text-left align-middle">
                            <span class="align-middle" title="Artist Name">{{ $artist->artist_name }}</span>
                            <span class="text-black-50 align-middle" title="Artist Owner">{{$artist->user['name']}} @if (Auth::user()->isAdmin()) ({{$artist->user['id']}}) @endif</span>
                            <a href="/artist/{{ $artist->id }}/songs" title="Manage Artist's Songs"><i class="fas fa-compact-disc edit-songs float-right pr-2 py-2"></i></a>
                            <a href="/music/{{urlencode($artist->artist_name)}}" title="Link to Artist Page"><i class="fas fa-play-circle text-dark float-right pr-2 py-2"></i></a>
                            @if (Auth::user()->isAdmin())
                                <a href="/artist/{{ $artist->id }}/delete" title="Delete Artist"><i class="fas fa-user-minus minus-user float-right pr-2 py-2"></i></a>
                            @endif

                            @if ((Auth::user()->isModerator() && Auth::user()->id === $artist->user_id) || Auth::user()->isAdmin())
                                <a href="/artist/{{ $artist->id }}/edit" title="Edit Artist"><i class="fas fa-user-edit edit-user float-right pr-2 py-2"></i></a>
                            @endif
                            <span class="text-black align-middle pr-2 py-1 float-right text-black-50" title="Number of Artist's Songs"><b><?php countSongs($artist->id) ?></b><i class="fas fa-music pl-1"></i></span>
                    </li>
                @endforeach
            </ul>
        </div>

        <!-- /////////////////////////////////////// -->
        <div class="row justify-content-center mx-0 mb-1 mt-5">
            <h2 class="w-100 mx-0">Upload</h2>
        </div>
        <div class="row justify-content-center w-100 mx-0 px-0">
            <form method="POST" action="/song/add" class="col-xs-12 col-sm-12 col-md-10 col-lg-7 col-xl-7 mb-3 justify-content-md-end row px-0">
                {{ csrf_field() }}
                {{ method_field('put') }}
                <div class="input-group mb-3 col-md-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Song</span>
                    </div>
                    <input name="title" type="text" class="form-control" placeholder="Title">
                </div>

                <div class="input-group mb-3 col-md-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Destination</span>
                    </div>
                    <select name="destination" type="text" class="form-control pl-2" placeholder="Destination">
                        <option value="Music" selected>Music</option>
                        <option value="Album">Album</option>
                        <option value="Project">Project</option>
                    <select>
                </div>

                <div class="input-group mb-3 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Feature</span>
                        </div>
                        <input name="feature" type="text" class="form-control" placeholder="Artist x Artist">
                </div>

                <div class="input-group mb-3 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Prodction</span>
                        </div>
                        <input name="production" type="text" class="form-control" placeholder="Producent">
                </div>

                <div class="input-group mb-3 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Album</span>
                        </div>
                        <input name="album" type="text" class="form-control" placeholder="Album Title">
                </div>

                <div class="input-group mb-3 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Tag</span>
                        </div>
                        <input name="tag" type="text" class="form-control" placeholder="Genre">
                </div>

                <div class="input-group mb-3 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Description</span>
                        </div>
                        <input name="description" type="text" class="form-control" placeholder="Short description">
                </div>

                <div class="input-group mb-3 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Embed</span>
                        </div>
                        <input name="embed" type="text" class="form-control" placeholder="Player Embeded">
                </div>

                <div class="input-group mb-3 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Image</span>
                        </div>
                        <input name="image" type="text" class="form-control" placeholder="Image Link">
                </div>

                <div class="input-group mb-3 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Link<i class="fab fa-soundcloud text-muted pl-3"></i></span>
                        </div>
                        <input name="link_sc" type="text" class="form-control" placeholder="SoundCloud Link">
                </div>

                <div class="input-group mb-3 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Link<i class="fab fa-facebook text-muted pl-3"></i></span>
                        </div>
                        <input name="link_fb" type="text" class="form-control" placeholder="Facebook Link">
                </div>

                <div class="input-group mb-3 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Link<i class="fab fa-youtube text-muted pl-3"></i></span>
                        </div>
                        <input name="link_yt" type="text" class="form-control" placeholder="YouTube Link">
                </div>

                <div class="row w-100 justify-content-center mx-0 px-3">
                    <button type="submit" class="btn btn-dark col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-2">Add</button>
                </div>
            </form>
        </div>
            <!-- /////////////////////////////////// -->
        <div class="row justify-content-center w-100 mx-0 mb-2 mt-5">
            <h2 class="w-100 mx-0">Songs</h2>
        </div>
        <div class="row justify-content-center w-100 mx-0">
            <ul class="list-unstyled justify-content-center col-xs-12 col-sm-12 col-md-10 col-lg-7 col-xl-7 pl-0 pr-0">
                @foreach ($data['songs'] as $song)
                @if($song->destination == 'Album')<li class="media mb-3 text-left bg-light">
                @else
                    @if($song->user->id == Auth::user()->id)<li class="media mb-3 text-left bg-light-blue">@endif
                    @if($song->user->id != Auth::user()->id)<li class="media mb-3 text-left bg-light-red">@endif
                @endif

                        <img src="{{ $song->image }}" class="mr-3 song-cover-img" alt="{{ $song->title }}">
                        <div class="media-body pl-3">
    
                            <div class="row w-100">
                            <div class="col-12 mt-2 mb-0 pl-0 text-truncate">
                                <span class="text-black-50">Title:</span>    
                                <span class="pl-0 mb-0 pr-0">{{ $song->title }}</span> 
                            </div>

                            <div class="col-12 pl-0 mb-0 pr-0 text-truncate">
                                <span class="text-black-50">Feat:</span>
                                <span class="pl-0 mb-0 pr-0">{{ $song->feature }} prod. {{$song->production}}</span>
                            </div>

                            <div class="col-6 pl-0 mb-0 pr-0 text-truncate">
                                <span class="text-black-50">Album:</span>
                                <span class="pl-0 mb-0 pr-0">{{ $song->album }}</span>
                            </div>

                            <div class="col-6 pl-0 mb-0 pr-0 text-truncate">
                                <span class="text-black-50">Tag:</span>
                                <span class="pl-0 mb-0 pr-0">{{ $song->tag }}</span>
                            </div>

                            <div class="col-6 pl-0 mb-0 pr-0 text-truncate">
                                <span class="text-black-50">Created:</span>
                                <span class="pl-0 mb-0 pr-0"><?php timePassed($song->created_at)?></span>
                            </div>

                            <div class="col-6 pl-0 mb-0 pr-0 text-truncate">
                                <!-- if($song->updated_at != null) -->
                                <span class="text-black-50">Updated:</span>
                                <span class="pl-0 mb-0 pr-0"><?php timePassed($song->updated_at)?></span>
                                <!-- endif -->
                            </div>

                            <div class="col-6 pl-0 mb-0 pr-0 text-truncate">
                                <span class="text-black-50">Destint:</span>
                                <span class="pl-0 mb-0 pr-0"><b>{{ $song->destination }}</b></span>
                            </div>

                            <div class="col-6 pl-0 mb-0 pr-0 text-truncate">
                                <span class="text-black-50">User:</span>
                                @if(Auth::user()->isAdmin())
                                <!-- jak usune uzytkownika to wywala blad  -->
                                    <span class=""><b>{{ $song->user->name }} </b></span>
                                    <span class="text-black-50">({{$song->user->id}})</span>
                                @endif

                                @if (Auth::user()->isModerator())
                                <span class=""><b>{{ $song->user->name }}</b></span>
                                @endif
                            </div>

                            <div class="col-12 pl-0 mb-0 pr-0">
                                
                                <span class="float-left text-black-50">Links:</span>
                                <span class="">
                                @if ($song->link_fb != null) <a href="{{$song->link_fb}}" target="_blank" class="icon-facebook text-black" title="Link to Facebook"><i class="fab fa-facebook"></i></a> @endif
                                @if ($song->link_yt != null) <a href="{{$song->link_yt}}" target="_blank" class="icon-youtube text-black" title="Link to YouTube"><i class="fab fa-youtube"></i></a> @endif
                                @if ($song->link_sc != null) <a href="{{$song->link_sc}}" target="_blank" class="icon-soundcloud text-black" title="Link to SoundCloud"><i class="fab fa-soundcloud"></i></a> @endif
                                
                                @if (Auth::user()->isAdmin()) <a href="/song/{{ $song->id }}/delete" title="Delete this song"><i class="fas fa-trash-alt minus-user px-1 pt-1 float-right"></i></a>@endif
                                @if (Auth::user()->isAdmin()) <a href="/song/{{ $song->id }}/edit" title="Edit this song"><i class="fas fa-edit edit-user px-1 pt-1 float-right"></i></a> @endif
                                @if (Auth::user()->isModerator() && $song->user->id == Auth::user()->id)<a href="/song/{{ $song->id }}/delete" title="Delete this song"><i class="fas fa-trash-alt minus-user px-1 pt-1 float-right"></i></a> @endif
                                @if (Auth::user()->isModerator() && $song->user->id == Auth::user()->id)<a href="/song/{{ $song->id }}/edit" title="Edit this song"><i class="fas fa-edit edit-user px-1 pt-1 float-right"></i></a> @endif
                                
                                </span>
                            </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endauth
@endif
@endsection