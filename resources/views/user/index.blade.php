@extends('layouts.master')
@section('title')<title>{{Auth::user()->name}}'s Profile - dopeBoyz</title>@endsection
@section('description')<meta name="description" content="dopeBoyz - music group associated with Trap and Hip-Hop genre. Listen to all our music projects and feel the vibes. Join us now!"/>@endsection

@section('content')
<div class="container mt-4">
    <div class="row justify-content-center w-100 mx-0">
        <h5 class="mb-4">Profile Information - {{Auth::user()->name}}</h5>
    </div>
    <div class="row justify-content-center w-100 mx-0">

        <form method="POST" action=" {{ route('users.update', Auth::user()) }} " class="col-10">
        {{ csrf_field() }}
        {{ method_field('patch') }}
        <div class="profile-container">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">First Name</span>
                </div>
                <input type="text" name="firstname" class="form-control" placeholder="Your First Name" aria-label="Username" aria-describedby="basic-addon1" value="{{ Auth::user()->firstname }}">
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Last Name</span>
                </div>
                <input type="text" name="surname" class="form-control" placeholder="Your Last Name" value="{{ Auth::user()->surname }}">
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Address</span>
                </div>
                <input type="text" name="address" class="form-control" placeholder="First line of the address" value="{{ Auth::user()->address }}">
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">City / Town</span>
                </div>
                <input type="text" name="city" class="form-control" placeholder="Your City" value="{{ Auth::user()->city }}">
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Postcode</span>
                </div>
                <input type="text" name="postcode" class="form-control" placeholder="Your Postcode" value="{{ Auth::user()->postcode }}">
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Phone</span>
                </div>
                <input type="text" name="phone" class="form-control" placeholder="Your Phone Number" value="{{ Auth::user()->phone }}">
            </div>

            <div class="row w-100 justify-content-center mx-0">
                <button type="submit" class="button-dark">Save</button>
                <div class="button-dark"><a href="\">Back</a></div>
            </div>
        </div>
        </form>

    </div>
</div>

<!-- <div class="card-body">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    You are logged in!
</div> -->
@endsection
