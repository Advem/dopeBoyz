@extends('layouts.master')
@section('title')<title>{{$user->name}} - User Edit - dopeBoyz</title>@endsection
@section('description')<meta name="description" content="dopeBoyz - music group associated with Trap and Hip-Hop genre. Listen to all our music projects and feel the vibes. Join us now!"/>@endsection

@section('content')
    <div class="container mt-4">
        <div class="row justify-content-center w-100 mx-0">
            <h5 class="mb-4">{{$user->name}}</h5>
        </div>
            <div class="row justify-content-center w-100 mx-0">

                    <form method="POST" action=" {{ route('users.update', $user) }} " class="col-10">
                    {{ csrf_field() }}
                    {{ method_field('patch') }}
                    <div class="profile-container">
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-light" id="basic-addon1">ID</span>
                            </div>
                            <input type="text" name="id" class="form-control text-black-50" value="{{ $user->id }}">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-light" id="basic-addon1">Email</span>
                            </div>
                            <input type="email" name="email" class="form-control text-black-50" value="{{ $user->email }}">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-light" id="basic-addon1">Name</span>
                            </div>
                            <input type="text" name="name" class="form-control text-black-50" value="{{ $user->name }}">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">First Name</span>
                            </div>
                            <input type="text" name="firstname" class="form-control" aria-label="Username" aria-describedby="basic-addon1" value="{{ $user->firstname }}">
                        </div>
            
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Last Name</span>
                            </div>
                            <input type="text" name="surname" class="form-control" value="{{ $user->surname }}">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Address</span>
                            </div>
                            <input type="text" name="address" class="form-control" value="{{ $user->address }}">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">City / Town</span>
                            </div>
                            <input type="text" name="city" class="form-control" value="{{ $user->city }}">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Postcode</span>
                            </div>
                            <input type="text" name="postcode" class="form-control" value="{{ $user->postcode }}">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Phone</span>
                            </div>
                            <input type="text" name="phone" class="form-control" value="{{ $user->phone }}">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-dark text-white border-dark" id="basic-addon1">Type</span>
                            </div>
                            <input type="text" name="type" class="form-control font-weight-bold" value="{{ $user->type }}">
                        </div>
            
                    
                        <div class="row w-100 justify-content-center mx-0">
                            <button type="submit" class="button-dark">Save</button>
                            <div class="button-dark"><a href="/user/{{$user->id}}/delete">Delete</a></div>
                            <div class="button-dark"><a href="/admin">Back</a></div>
                        </div>
                    </div>
                    </form>
                </div>
    </div>
@endsection