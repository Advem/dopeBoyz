@extends('layouts.master')
@section('title')<title>{{$data['artist']->artist_name}} on dopeBoyz - Listen to {{$data['artist']->artist_name}}'s Tracks now!</title>@endsection
@section('description')<meta name="description" content="Listen to {{$data['artist']->artist_name}}'s tracks on dopeBoyz and feel the vibes now!"/>@endsection
@section('content')
    @include('layouts.topNav')
    @include('layouts.backgroundSign')
    <div class="mt-4"></div>
    @include('layouts.songsList')
    <div class="no-spacing w-97"></div>
@endsection
