@extends('layouts.master')
@section('title')<title>{{$data['title']}} on dopeBoyz - Listen to this Track now!</title>@endsection
@section('description')<meta name="description" content="Listen to {{$data['title']}} Track on dopeBoyz.net and more..."/>@endsection


@section('content')
    @include('layouts.topNav')
    @include('layouts.backgroundSign')
    <div class="mt-4"></div>
    @include('layouts.songsList')
    <div class="row w-100 justify-content-center mx-0 mt-2 anim-home-menu">
        <div class="button-dark2"><a href="/music/{{urlencode($data['artist'])}}">More {{$data['artist']}}'s Tracks</a></div>
    </div>
@endsection