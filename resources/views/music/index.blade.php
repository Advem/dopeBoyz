@extends('layouts.master')
@section('title')<title>{{$data['newestAlbum']->title}} on dopeBoyz - Listen to the Newest Album now!</title>@endsection
@section('description')<meta name="description" content="Check our Newest Album - {{ $data['newestAlbum']->title }} on dopeBoyz Music Page. Listen whole playlist via SoundCloud player or dopeBoyz Songs Layout."/>@endsection
@section('content')
    <div class="page-music z-index--10">
        @include('layouts.topNav')
        <img src="/images/logo/dopeBoyzLogo2019sign.png" class="img-responive d-block mx-auto dopeboyz-sign z-index-00" alt="dopeBoyz Sign">
            @if ($data['count'] > 0)
            @foreach ($data['albums'] as $song)
                <div class="row justify-content-center mx-0 my-0 anim-song w-100 mt-4">
                    <div class="album-field col-xs-12 col-sm-10 col-md-8 col-lg-8 col-xl-4 w-100">
                    <h2 class="album-subtitle pt-3 pb-0 m-0">Newest Album</h2>
                    <h1 class="album-title m-0 pb-3 pt-1 notranslate">{{$song->title}}</h1>
                    </div>
                        <div class="row justify-content-center pb-0 w-100 mx-0">
                            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8 col-xl-4 pr-0 pl-0">
                                <iframe class="player-music w-100 px-0" width="100%" scrolling="no" frameborder="no" allow="autoplay" src="{{ $song->embed }}"></iframe>
                            </div>
                        </div>
                </div>
            @endforeach
            @endif
            
            @if ($data['count'] > 0)
            @include('layouts.songsList')
            @endif
            <div class="no-spacing w-97"></div>
    </div>
@endsection