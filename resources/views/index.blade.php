<!-- dopeBoyz - Home Page -->
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @section('title')<title>dopeBoyz - Listen to Trap Music - Feel the vibes now!</title>@endsection
    @section('description')<meta name="description" content="dopeBoyz - music group associated with Trap and Hip-Hop genre. Listen to all our music projects and feel the vibes. Join us now!"/>@endsection

    @include('layouts.head')
    <body class="body-home">

    @include('layouts.socialLinks')

    <div class="w-100 d-flex justify-content-start mx-0 my-0 fixed-bottom anim-home-logo">
          <span class="ml-2 text-muted">Last Update: 21052019 2007</span>
      </div>

        <div class= "d-flex justify-content-center align-middle" id="home-bg">
            <header>
            <img src="/images/logo/dopeBoyzLogo2019sign.png" class="img-responive d-block mx-auto" id="about-img-sign" alt="dopeBoyz Sign">
            <img src="/images/logo/dopeBoyzLogo2019sign.png" class="img-responsive d-block mx-auto anim-home-logo-sign" id="home-img-sign" alt="dopeBoyz Sign" title="dopeBoyz">
            <h1>
                <a href="/">
                    <img src="/images/logo/dopeBoyzLogo2019title.png" class="img-responive d-block mx-auto anim-home-logo" id="home-img-title" alt="dopeBoyz Logo" title="dopeBoyz">
                </a>
            </h1>
            </header>

        <nav class="navbar navbar-expand-sm justify-content-center anim-home-menu" id="home-menu-text">
        <button class="navbar-toggler float-righ w-100 js-arrow" id="home-menu-colapse-button" type="button" data-toggle="collapse" data-target="#navbarsExample10" aria-controls="navbarsExample10" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon" id="home-menu-arrow"><i class="fas fa-angle-down rotate"></i></span>
        </button>

        <div class="collapse navbar-collapse justify-content-center" id="navbarsExample10">
          <ul class="navbar-nav d-flex justify-content-center" id="home-menu-link-field">
            <li class="nav-item d-flex justify-content-center home-menu-link"><h2 class="h-home"><a class="nav-link hover-no-bg" href="/about">About</a></h2></li>
            <li class="nav-item d-flex justify-content-center home-menu-link"><h2 class="h-home"><a class="nav-link hover-no-bg" href="/music">Music</a></h2></li>
            <li class="nav-item d-flex justify-content-center home-menu-link"><h2 class="h-home"><a class="nav-link hover-no-bg" href="/graphics">Graphics</a></h2></li>
            <li class="nav-item d-flex justify-content-center home-menu-link"><h2 class="h-home"><a class="nav-link hover-no-bg" href="/shop">Shop</a></h2></li>
            <li class="nav-item d-flex justify-content-center home-menu-link"><h2 class="h-home"><a class="nav-link hover-no-bg" href="/contact">Contact</a></h2></li>
    
            <div class="nav-item dropdown show d-flex justify-content-center">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                </a>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" id="home-menu-dropdown">
                @if (Route::has('login'))
                  @auth
                  <h3 class="h-home"><a class="dropdown-item home-menu-dropdown-item" href="{{ url('/user') }}">Profile</a></h3>
                    @if (Auth::user()->isAdmin() || Auth::user()->isModerator())
                    <h3 class="h-home"><a class="dropdown-item home-menu-dropdown-item"  href="/project">Project</a></h3> @endif
                    @if (Auth::user()->isAdmin()) 
                    <h3 class="h-home"><a class="dropdown-item home-menu-dropdown-item" href="/admin">Admin Panel</a></h3> @endif
                    @if (Auth::user()->isModerator()) 
                    <h3 class="h-home"><a class="dropdown-item home-menu-dropdown-item" href="/admin">Mod Panel</a></h3> @endif
                    <h3 class="h-home"><a class="dropdown-item home-menu-dropdown-item" href="/documentation">Documentation</a></h3>
                    <h3 class="h-home"><a class="dropdown-item home-menu-dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log out</a></h3>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                  @else
                      <h3 class="h-home"><a class="dropdown-item home-menu-dropdown-item" href="{{ route('login') }}">Log in</a></h3>
                      @if (Route::has('register'))
                      <h3 class="h-home"><a class="dropdown-item home-menu-dropdown-item" href="{{ route('register') }}">Register</a></h3> @endif
                      <h3 class="h-home"><a class="dropdown-item home-menu-dropdown-item" href="/support">Support</a></h3>
                  @endauth
                @endif
                </div>
            </div>
          </ul>
        </div>
      </nav>
    </body>
</html>