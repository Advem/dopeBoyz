@extends('layouts.master')
@section('title')
<title>{{$data['artist']->artist_name}} - Songs</title>
@endsection
@include('layouts.functions')
@section('content')
    <div class="container">
        <h2 class="pt-4">{{ $data['artist']->artist_name }}</h2>

        <ul class="list-group mb-5 songs-list">
            @foreach ($data['artist']->songs as $song)
                <li class="list-group-item list-group-item-primary text-left">
                    <span class="d-inline-block text-truncate">{{$song->title}}</span>
                    <span class="d-inline-block text-truncate">ft. {{$song->feature}}</span>
                    <span class="d-inline-block text-truncate">prod. {{$song->production}}</span>
                    <span class="d-inline-block text-truncate">Album: {{$song->album}}</span>      
                    <span class="d-inline-block text-truncate">User: <b>{{$song->user->name}}</b></span>
                    @if ($song->user_id === Auth::user()->id || Auth::user()->isAdmin() || $data['artist']->user_id === Auth::user()->id)
                    <a href="/artist/{{ $data['artist']->id }}/disassoc/{{ $song->id }}" title="Detach this song from {{ $data['artist']->artist_name }}">
                        <i class="fas fa-trash-alt minus-user float-right py-1 px-2"></i>
                    </a>
                    @endif
                </li>
            @endforeach
        </ul>

        <h2 class="pt-4">My Songs</h2>

        <ul class="list-group songs-list">
                @foreach ($data['songs'] as $song)
                    <li class="list-group-item list-group-item-success text-left">
                    <!-- <img src="{{ $song->image }}" class="mr-3" alt="{{ $song->title }}"> -->
                    <span class="d-inline-block text-truncate">{{$song->title}}</span>
                    <span class="d-inline-block text-truncate">feat. {{$song->feature}}</span>
                    <span class="d-inline-block text-truncate">prod. {{$song->production}}</span>
                    <span class="d-inline-block text-truncate">Album: {{$song->album}}</span>      
                    <span class="d-inline-block text-truncate">Destination: <b>{{ $song->destination}}</b></span>
                    <span class="d-inline-block text-truncate">User: <b>{{$song->user->name}}</b></span>
                    @if ($song->destination == 'Music' && belogsToArtist($song->id, $data['artist']->id) == FALSE)
                        <a href="/artist/{{ $data['artist']->id }}/assoc/{{ $song->id }}" title="Attach this song to {{ $data['artist']->artist_name }}">
                            <i class="fas fa-plus-circle edit-user float-right py-1 px-2"></i>
                        </a>
                    @endif
                    </li>
                @endforeach
        </ul>

                        <div class="row w-100 justify-content-center mx-0 mt-5 px-5">
                            <div class="button-dark"><a href="/admin">Back</a></div>
                        </div>

    </div>
@endsection