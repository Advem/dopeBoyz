@extends('layouts.master')
@section('title')
<title>{{$artist->artist_name}} - Artist Edit</title>
@endsection
@section('content')
    <div class="container mt-4">
            <div class="row justify-content-center mr-0">

                    <form method="POST" action=" {{ route('artist.update', $artist) }} " class="row col-md-6 mb-3 px-5 mr-0 justify-content-center">
                        {{ csrf_field() }}
                        {{ method_field('patch') }}
                        <div class="input-group mb-3 px-0 col-md-12">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Artist name</span>
                            </div>
                            <input type="text" name="artist_name" class="form-control" placeholder="New arist name..." value="{{ $artist->artist_name }}">
                        </div>

                        @if (Auth::user()->isAdmin())
                        <div class="input-group mb-3 px-0 col-md-12">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Position</span>
                            </div>
                            <input type="text" name="position" class="form-control" placeholder="Set position..." value="{{ $artist->position }}">
                        </div>

                        <div class="input-group mb-3 px-0 col-md-12">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">User</span>
                            </div>
                            <input type="text" name="user_id" class="form-control" placeholder="Link user..." value="{{ $artist->user_id }}">
                            @endif
                        </div>
            
                        <div class="row w-100 justify-content-center mx-0">
                            <button type="submit" class="button-dark">Save</button>
                            <div class="button-dark"><a href="/admin">Back</a></div>
                        </div>
                    </form>
            
                </div>
    </div>
@endsection