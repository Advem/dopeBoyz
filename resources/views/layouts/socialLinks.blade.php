<!-- @foreach($links as $link)
      <div class="w-100 d-flex justify-content-center mx-0 fixed-bottom anim-home-links">
          <div class="d-flex justify-content-center align-middle col-sm-6 col-md-4 col-lg-4 col-xl-2" id="home-hyperlinks"> 
            @if ($link->link_fb != null) <a href="{{$link->link_fb}}" target="_blank" class="home-icon"><i class="fab fa-facebook-square"></i></a> @endif
            @if ($link->link_tw != null) <a href="{{$link->link_tw}}" target="_blank" class="home-icon"><i class="fab fa-twitter-square"></i></a> @endif
            @if ($link->link_yt != null) <a href="{{$link->link_yt}}" target="_blank" class="home-icon"><i class="fab fa-youtube-square"></i></a> @endif
            @if ($link->link_ig != null) <a href="{{$link->link_ig}}" target="_blank" class="home-icon"><i class="fab fa-instagram"></i></a> @endif
            @if ($link->link_sc != null) <a href="{{$link->link_sc}}" target="_blank" class="home-icon"><i class="fab fa-soundcloud"></i></a> @endif
          </div>
      </div>
@endforeach -->

      <div class="w-100 d-flex justify-content-center mx-0 fixed-bottom anim-home-links">
          <div class="d-flex justify-content-center align-middle col-sm-6 col-md-4 col-lg-4 col-xl-2" id="home-hyperlinks"> 
            <a href="https://www.facebook.com/dopeboyzFAMILIA" target="_blank" class="home-icon" title="dopeBoyz on Facebook"><i class="fab fa-facebook-square"></i></a>
            <a href="https://twitter.com" target="_blank" class="home-icon"><i class="fab fa-twitter-square" title="dopeBoyz on Twitter"></i></a>
            <a href="https://youtube.com" target="_blank" class="home-icon"><i class="fab fa-youtube-square" title="dopeBoyz on YouTube"></i></a>
            <a href="https://instagram.com" target="_blank" class="home-icon"><i class="fab fa-instagram" title="dopeBoyz on Instagram"></i></a>
            <a href="https://soundcloud.com/blessedsupremesaintsofroswell" target="_blank" class="home-icon" title="dopeBoyz on SoundCloud"><i class="fab fa-soundcloud"></i></a>
          </div>
      </div>
