<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
    <body>
        <div class="position-ref full-height">
        @include('layouts.nav')
            <div class="content">
                @yield('content')
            </div>
            <!-- @include('layouts.footer') -->
        </div>
    </body>
</html>