<?php 
    function timePassed($date) {

    $mytime = Carbon\Carbon::now();
    // echo $mytime->toDateTimeString().'<br>';

    $start_date = new DateTime($date);
    $since_start = $start_date->diff(new DateTime($mytime->toDateTimeString()));
                                
    if ($since_start->y >= 1) {
        if ($since_start->y == 1) echo $since_start->y.' year ago';
        else echo $since_start->y.' years ago';
    }
    else if ($since_start->y == 0 && $since_start->m >= 1 and $since_start->m <= 12) {
        if ($since_start->m == 1) echo $since_start->m.' month ago';
        else echo $since_start->m.' months ago';
    }
    else if ($since_start->y == 0 && $since_start->m == 0 && $since_start->d > 0 && $since_start->d <= 31) {
        if ($since_start->d == 1) echo $since_start->d.' day ago';
        else echo $since_start->d.' days ago';
    }
    else if ($since_start->y == 0 && $since_start->m == 0 && $since_start->d == 0 && $since_start->h > 0 && $since_start->h <= 24) {
        if ($since_start->h == 1) echo $since_start->h.' hour ago'; 
        else echo $since_start->h.' hours ago'; 
    }
    else if ($since_start->y == 0 && $since_start->m == 0 && $since_start->d == 0 && $since_start->h == 0 && $since_start->i <= 60 && $since_start->i > 0) {
        if ($since_start->i == 1) echo $since_start->i.' min ago';
        else echo $since_start->i.' mins ago';
    }
    else if ($since_start->y == 0 && $since_start->m == 0 && $since_start->d == 0 && $since_start->h == 0 && $since_start->i == 0 && $since_start->s <= 60) 
    if ($since_start->s != 0) echo $since_start->s.' sec ago';
    
}

    function userName($id) {

        $user = App\User::where('id', $id)->first();
        echo $user->name;
    }

    function belogsToArtist($song_id, $artist_id) {

        $exists = DB::table('artist_song')
        ->whereArtistId($artist_id)
        ->whereSongId($song_id)
        ->count();

        if ($exists > 0) return true;
        else return false;
    }

    function countSongs($artist_id) {

        $number = DB::table('artist_song')
        ->whereArtistId($artist_id)
        ->count();

        echo $number;
    }

    function isFresh($date) {

        $mytime = Carbon\Carbon::now();
    
        $start_date = new DateTime($date);
        $since_start = $start_date->diff(new DateTime($mytime->toDateTimeString()));
                                    
        if ($since_start->d <= 7 && $since_start->m == 0 && $since_start->y == 0) return true;
        else return false;
    }
    
?>                                                   
