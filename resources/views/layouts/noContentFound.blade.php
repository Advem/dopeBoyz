@extends('layouts.master')
@section('title')
<title>Error - no such content found</title>
@endsection

@section('content')
    <img src="/images/logo/dopeBoyzLogo2019signBlack.png" class="img-responive d-block mx-auto" id="about-img-sign" alt="dopeBoyz Sign">
    <div class="container h-100 my-auto align-middle col-10 col-sm-10 col-md-8 col-lg-7 col-xl-5 px-2 anim-song mt-4">
        <h1 class="mt-4">Error!</h1>
        <h3>No such content found...</h3>
    </div>

    <div class="row w-100 justify-content-center mx-0 mt-2 mb-3 anim-home-menu">
        <div class="button-dark2"><a href="/music/dopeBoyz">More dopeBoyz's Tracks</a></div>
    </div> 

    <div class="no-spacing w-97"></div>
@endsection