<div class="d-flex row justify-content-center mx-0">
    <!-- if number of artist is too big for this nav to be scalable no mobile phones -> change class "navbar-expand-sm" to "navbar-expand-md" -->
<nav class="navbar navbar-expand-sm py-0 justify-content-center w-100 anim-top-nav z-index-topnav" id="artist-menu-text">
        <button class="navbar-toggler w-100 js-arrow" id="home-menu-colapse-button" type="button" data-toggle="collapse" data-target="#navbarsExample11" aria-controls="navbarsExample11" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon py-3" id="artist-menu-arrow"><i class="fas fa-angle-down rotate"></i></span>
        </button>

        <div class="collapse navbar-collapse justify-content-center" id="navbarsExample11">
          <ul class="navbar-nav d-flex justify-content-center" id="artist-menu-link-field">
            @foreach ($data['artists'] as $artist)
                <li class="d-flex justify-content-center" id="artist-menu-link"><a class="" href="/music/{{ urlencode($artist->artist_name) }}">{{ $artist->artist_name }}</a></li>
            @endforeach
            <li class="d-flex justify-content-center display-none" id="artist-menu-link"><a class=""></a></li>
          </ul>
        </div>
      </nav>
</div>
