@include('layouts.functions')
<div class="row justify-content-center container w-100 mx-auto col-xs-12 col-sm-10 col-md-8 col-lg-8 col-xl-4">    
        <div class="row justify-content-center mx-0 w-100 pb-4">
            <ul class="nav nav-pills list-unstyled w-100" id="pills-tab" role="tablist">
                @foreach ($data['songs'] as $song)
                    <li class="nav-item media text-left song anim-song">
                            <div class="nav-link d-flex justify-content-center w-100" alt="{{ $song->title }} Cover" id="pills-{{$song->id}}-tab" 
                                data-toggle="pill" href="#pills-{{$song->id}}" role="tab" aria-controls="pills-{{$song->id}}" aria-selected="false" title="Listen to this track!">

                                <div class="mr-3 song-cover">
                                <img alt="Song Cover" title="Listen to this track!" src="{{ $song->image }}" class="song-cover-img"></div>

                                @if (Request::is('music') == false) @if (isFresh($song->created_at))
                                    <div class="justify-content-center mx-0 my-0 icon-new">
                                        <span class="pl-1 default-font">Fresh!</span>
                                    </div>
                                @endif @endif

                                <div class="justify-content-center mx-0 my-0 icon-play"><span class="fas fa-play mx-0 my-0 w-100 h-100" alt="play button"></span></div>
            
                                <div class="media-body pr-0 song-body" title="Listen to this track!">
                                    <span class="d-flex justify-content-end float-right tags-field py-0 pt-2 pr-1">
                                        <div class="text-black-50 tag-time px-0"><?php timePassed($song->created_at)?></div>
                                        <span class="d-inline justify-content-center badge tag badge-secondary text-black-75 text-truncate notranslate">{{ $song->tag }}</span>
                                    </span>
                                    <h1 class="overflow-hidden mt-2 mb-2 song-title notranslate">{{ $song->title }}</h1>
                                    <h2 class="mt-0 mb-2 text-black-75 song-production notranslate">{{ $song->feature }} prod. {{ $song->production }}</h2>
                                    @if ($song->description == 'countdown27Club') <h3 class="mt-2 mb-2 w-75 song-description text-black-50" id="countdown27Club">{{ $song->description }}</h3>
                                    @else <h3 class="mt-2 mb-2 w-75 song-description text-black-50">{{ $song->description }}</h3> @endif             
                                </div>
                            </div>

                            <div class="d-flex justfy-content-start fixed-bottom song-links text-truncate px-0 pb-2 w-97">
                                @auth 
                                    <!-- @if (Auth::user()->isAdmin() || Auth::user()->isModerator())<div class="d-flex justfy-content-start fixed-bottom song-links col-5 text-truncate px-0"> @endif  -->
                                    @if (Auth::user()->isModerator()) <div class="text-black-50 default-font pr-1 pl-1"><?php userName($song->user_id)?></div> @endif
                                    @if (Auth::user()->isAdmin()) <div class="text-black-50 default-font pr-1 pl-1"><?php userName($song->user_id)?> ({{$song->user_id}})</div> @endif
                                @endauth            
                                        @if ($song->title != NULL) <a href="/song/{{urlencode($song->title)}}" title="Link to this track"><i class="fas fa-music px-1 pt-1 icon-dark"></i></a> @endif
                                        @if ($song->album != NULL) <a href="/album/{{urlencode($song->album)}}" title="Link to {{ $song->album }} Album"><i class="fas fa-compact-disc px-1 pt-1 icon-dark"></i></a> @endif 
                                @auth
                                    @if (Auth::user()->isModerator() && $song->user_id == Auth::user()->id) <a href="/song/{{ $song->id }}/edit" title="Edit this song"><i class="fas fa-edit px-1 pt-1 icon-facebook"></i></a> @endif
                                    @if (Auth::user()->isModerator() && $song->user_id == Auth::user()->id) <a href="/song/{{ $song->id }}/delete" title="Delete this song"><i class="fas fa-trash-alt px-1 pt-1 icon-youtube"></i></a> @endif
                                    @if (Auth::user()->isAdmin()) <a href="/song/{{ $song->id }}/edit" title="Edit this song"><i class="fas fa-edit px-1 pt-1 icon-facebook"></i></a> @endif
                                    @if (Auth::user()->isAdmin()) <a href="/song/{{ $song->id }}/delete" title="Delete this song"><i class="fas fa-trash-alt px-1 pt-1 icon-youtube"></i></a> @endif
                                @endauth
                                    @if ($song->link_fb != null) <a href="{{$song->link_fb}}" title="{{ $song->title }} on Facebook" target="_blank" class="icon-facebook"><i class="fab fa-facebook"></i></a> @endif
                                    @if ($song->link_yt != null) <a href="{{$song->link_yt}}" title="{{ $song->title }} on YouTube" target="_blank" class="icon-youtube"><i class="fab fa-youtube"></i></a> @endif
                                    @if ($song->link_sc != null) <a href="{{$song->link_sc}}" title="{{ $song->title }} on SoundCloud" target="_blank" class="icon-soundcloud"><i class="fab fa-soundcloud"></i></a> @endif
                            </div>
                    </li>
                @endforeach
            </ul> 
        </div>
    </div>

    <!-- soundcloud player -->

    <!-- za pomoca js dodaj klase fixed-bottom do player-filed jak gosciu kliknie na songa -->
    <div class="row justify-content-center w-100 player-field"> 
        <div class="tab-content col-xs-12 col-sm-10 col-md-8 col-lg-8 col-xl-4 pr-0 pl-0" id="pills-tabContent">
                @foreach ($data['songs'] as $song)
                    <div class="tab-pane fade"  id="pills-{{$song->id}}" role="tabpanel" aria-labelledby="pills-{{$song->id}}-tab">
                        <iframe class="player w-100" height="165" scrolling="no" frameborder="no" allow="autoplay" src="{{ $song->embed }}"></iframe>
                    </div>
                @endforeach
        </div>
    </div>