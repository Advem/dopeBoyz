<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140700742-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-140700742-1');
        </script>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="author" content="Adam Drabik (Advem)"/>
        <meta name="designer" content="Adam Darbik (Advem)"/>
        <meta name="theme-color" content="#272727"/>
        <meta name="title" content="dopeBoyz - Trap / HipHop Music Group - Feel the vibes now!"/>
        <meta name="robots" content="index, follow"/>
        <meta name="subject" content="music"/>
        <meta name="copyright"content="dopeBoyz"/>
        <meta name="url" content="https://dopeboyz.net"/>
        <meta name="og:title" content="dopeBoyz - Trap / HipHop Music Group - Feel the vibes now!"/>
        <meta name="og:description" content="dopeBoyz - music group associated with Trap and Hip-Hop genre. Listen to all our music projects and feel the vibes. Join us now!"/>
        <meta name="og:url" content="https://dopeboyz.net"/>
        <meta name="og:image" content="https://dopeboyz.net/favicon.ico"/>
        <meta name="fb:page_id" content="1352440061542158"/>
        <meta name="og:type" content="website"/>
        @yield('title')
        @yield('description')
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <!-- Scripts -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        @yield('script')
        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com"/>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=ZCOOL+QingKe+HuangYou|Pathway+Gothic+One|Roboto+Condensed|Oswald:300" rel="stylesheet" type="text/css"/>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
        <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"/>
        <link rel="canonical" href="https://dopeboyz.net/"/>
</head>
