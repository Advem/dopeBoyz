<!-- main menu -->
<div class="sticky-top mb-0 z-index-menu">
<nav class="navbar navbar-expand-sm py-0 px-0" id="main-menu-text">
    <a href="/" class= "ml-3" id="main-img-title-a"><img src="/images/logo/dopeBoyzLogo2019title.png" class="justify-content-start" id="main-img-title" alt="dopeBoyz Logo" href="/"></a>
        <button class="navbar-toggler float-right js-arrow" id="main-menu-colapse-button" type="button" data-toggle="collapse" data-target="#navbarsExample10" aria-controls="navbarsExample10" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon mx-3" id="main-menu-arrow"><i class="fas fa-angle-down rotate"></i></span>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarsExample10">
          <ul class="navbar-nav d-flex justify-content-center" id="home-menu-link-field">
            <li class="d-flex justify-content-center" id="main-menu-link"><a class="" href="/about">About</a></li>
            <li class="d-flex justify-content-center" id="main-menu-link"><a class="" href="/music">Music</a></li>
            <li class="d-flex justify-content-center" id="main-menu-link"><a class="" href="/graphics">Graphics</a></li>
            <li class="d-flex justify-content-center" id="main-menu-link"><a class="" href="/shop">Shop</a></li>
            <li class="d-flex justify-content-center" id="main-menu-link"><a class="" href="/contact">Contact</a></li>
    
            <div class="dropdown collapse show d-flex justify-content-center">
                <a class="dropdown-toggle px-0 py-0 pt-2 mx-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                </a>

                <div class="dropdown-menu py-0" aria-labelledby="dropdownMenuLink" id="main-menu-dropdown">
                @if (Route::has('login'))
                  @auth
                    <a class="dropdown-item" id="home-menu-dropdown-item" href="{{ url('/user') }}">Profile</a>
                    @if (Auth::user()->isAdmin() || Auth::user()->isModerator()) 
                    <a class="dropdown-item" id="home-menu-dropdown-item" href="/project">Project</a> @endif
                    @if (Auth::user()->isAdmin()) <a class="dropdown-item" id="home-menu-dropdown-item" href="/admin">Admin Panel</a> @endif
                    @if (Auth::user()->isModerator()) <a class="dropdown-item" id="home-menu-dropdown-item" href="/admin">Mod Panel</a> @endif
                    <a class="dropdown-item" id="home-menu-dropdown-item" href="/documentation">Documentation</a>
                      <a class="dropdown-item" id="home-menu-dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log out</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                      </form>
                  @else
                      <a class="dropdown-item" id="home-menu-dropdown-item" href="{{ route('login') }}">Log in</a>
                      @if (Route::has('register'))
                      <a class="dropdown-item" id="home-menu-dropdown-item" href="{{ route('register') }}">Register</a> @endif
                      <a class="dropdown-item" id="home-menu-dropdown-item" href="/support">Support</a>
                  @endauth
                @endif
                </div>
            </div>
          </ul>
        </div>
      </nav>
</div>

<div id="main-menu-spacing"></div>
