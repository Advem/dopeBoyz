
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap');
//require('./bootstrap.bundle.min.js');
//require('jquery');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 window.onload = countdown27Club();

//User searching
var typingTimeout;
$('#userSearch').keyup(function () { //Start timout
    clearTimeout(typingTimeout);
    typingTimeout = setTimeout(() => { searchUser(this.value); }, 500);
});

$('#userSearch').keydown(function () {
    clearTimeout(typingTimeout);
});

async function searchUser(userFind) {
    if (userFind) {
        let url = `/user/${userFind}`
        $.getJSON(url, function (response) {
            if (response) {
                $(".users-list").html("");
                response.forEach((user) => {
                    $(".users-list").append(`
                    <li class="list-group-item">
                        UID ${user.id} | ${user.name} | ${user.email} | ${user.type}
                         
                        <a href="/user/${user.id}/delete"><i class="fas fa-user-minus minus-user float-right py-1 ml-1"></i></a>
                        <a href="/user/${user.id}/edit"><i class="fas fa-user-edit edit-user float-right py-1 ml-1"></i></a>
                    </li>`);
                });
            }
        });
    }
}

    $("#main-menu-link a").each(function() {
        console.log($(this).attr('href'));
        if ((window.location.pathname.indexOf($(this).attr('href'))) > -1) {
            $(this).addClass('active-menu');
        }
    });

    $("#artist-menu-link a").each(function() {
        console.log($(this).attr('href'));
        if ((window.location.pathname.indexOf($(this).attr('href'))) > -1) {
            $(this).parent().addClass('active-artist-bg');
        }
    });

    $("#artist-menu-link a").each(function() {
        console.log($(this).attr('href'));
        if ((window.location.pathname.indexOf($(this).attr('href'))) > -1) {
            $(this).addClass('active-artist');
        }
    });

    $(".js-arrow").click(function(){
        $(this).children().children().toggleClass("down")  ; 
       })

    $(".song").click(function(){
        $(".player-field").addClass("fixed-bottom").addClass("h-165")  ; 
       })

    $(".song").click(function(){
        $(".no-spacing").addClass("song-spacing")  ; 
       })
       
    if(document.URL.indexOf("/about") >= 0){ 
            $(".content").addClass('about-background');
            $("body").addClass('about-background');
        }
    
    else if(document.URL.indexOf("/shop") >= 0){ 
            $("body").addClass('about-background');
        }

    else if(document.URL.indexOf("/contact") >= 0){ 
            $("body").addClass('about-background');
        }

    else if(document.URL.indexOf("/") >= 0){
        $(".page-music").addClass('music-background');
    }

function countdown27Club() {
        
    var countDownDate = new Date("Sep 13, 2024 00:00:00").getTime();

    setInterval(function() {
        var now = new Date().getTime();
        var distance = countDownDate - now;

        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var mins = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var secs = Math.floor((distance % (1000 * 60)) / 1000);

        var addZero = ['', '', ''];

        if (hours < 10) addZero[0] = '0';
        else addZero[0] = '';

        if (mins < 10) addZero[1] = '0';
        else addZero[1] = '';

        if (secs < 10) addZero[2] = '0';
        else addZero[2] = '';

        document.getElementById("countdown27Club").innerHTML = '<i><b>' + days + '</b>d <b>' + addZero[0] + hours + "</b>h <b>" + addZero[1] + mins + '</b>min <b>' + addZero[2] + secs + '</b>s</i>';

        if (distance < 0) document.getElementById("countdown27Club").innerHTML = "HAPPY 27th Birthday Dawgo!";
    }, 1000);
        
}